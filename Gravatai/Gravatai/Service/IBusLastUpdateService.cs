﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Service
{
    public interface IBusLastUpdateService
    {
        Task<Dictionary<string, DateTime>> GetBusLastUpdates();
    }
}
