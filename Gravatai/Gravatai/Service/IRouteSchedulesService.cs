﻿using Gravatai.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Service
{
    public interface IRouteSchedulesService
    {
        Task<List<RouteSchedule>> GetRouteSchedules(int routeId, int directionType, int weekday);
    }
}
