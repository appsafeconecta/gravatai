﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gravatai.Model;

namespace Gravatai.Service
{
    public class PlacesService : IPlacesService
    {
        public async Task<List<Place>> GetPlaces(string keyword)
        {
            List<Place> places = new List<Place>();
            using (var client = new System.Net.Http.HttpClient())
            {
                string url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyAXw1AnIjGj5Vs8MAFaip9XzdjGksAD4VA&location=-29.950471,-50.983270&keyword=" + keyword + "&language=pt-BR&rankby=distance";
                var json = await client.GetStringAsync(url);
                GooglePlacesResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<GooglePlacesResult>(json);
                foreach (var place in result.Results)
                {
                    places.Add(new Place(place.Id, place.Name, new GeographicLocation(place.Geometry.Location.Lat, place.Geometry.Location.Lng), place.Vicinity));
                }
            }
            return places;
        }
    }

    internal class GooglePlacesResult
    {
        public string Status { get; set; }
        public List<GooglePlace> Results { get; set; }
    }

    internal class GooglePlace
    {
        public Geometry Geometry { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Vicinity { get; set; }
    }

    internal class Geometry
    {
        public Location Location { get; set; }
    }

    internal class Location
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
