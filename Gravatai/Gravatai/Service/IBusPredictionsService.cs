﻿using Gravatai.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Service
{
    public interface IBusPredictionsService
    {
        Task<List<BusPrediction>> GetBusPredictions(string url);
    }
}
