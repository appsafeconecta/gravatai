﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gravatai.Model;

namespace Gravatai.Service
{
    public class RouteStopsService : IRouteStopsService
    {
        public async Task<List<RouteStop>> GetRouteStops(int routeId)
        {
            List<RouteStop> routeStops = new List<RouteStop>();
            using (var client = new System.Net.Http.HttpClient())
            {
                string authUrl = "http://scgen.com.br/WebServices/Binder/Transoft/AutenticarUsuario?usuario=wsrafael.pontes&senha=r@f@3lp0nt3s";
                var authJson = await client.GetStringAsync(authUrl);
                Authentication auth = Newtonsoft.Json.JsonConvert.DeserializeObject<Authentication>(authJson);
                if (!auth.RetornoOK)
                    return null;
                string routeStopsUrl =
                    "http://scgen.com.br/WebServices/Binder/Transoft/ObterPontosLinha?guidIdentificacao=" +
                    auth.IdentificacaoLogin +
                    "&idEmpresa=1563&idLinha=" + routeId;
                var json = await client.GetStringAsync(routeStopsUrl);
                List<PontoLinha> pontosLinha = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PontoLinha>>(json);

                foreach (var pontoLinha in pontosLinha)
                {
                    routeStops.Add(new RouteStop(pontoLinha.Id, pontoLinha.Ordem, pontoLinha.Sentido, pontoLinha.Descricao));
                }

            }
            return routeStops;
        }
    }

    internal class PontoLinha
    {
        public int Id { get; set; }
        public int Ordem { get; set; }
        public int Sentido { get; set; }
        public string Descricao { get; set; }
    }
}
