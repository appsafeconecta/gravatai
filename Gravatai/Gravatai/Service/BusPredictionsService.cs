﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gravatai.Model;

namespace Gravatai.Service
{
    public class BusPredictionsService : IBusPredictionsService
    {
        public async Task<List<BusPrediction>> GetBusPredictions(string url)
        {
            List<BusPrediction> busPredictions = new List<BusPrediction>();
            using (var client = new System.Net.Http.HttpClient())
            {
                var json = await client.GetStringAsync(url);
                if (json.StartsWith("<!DOCTYPE html>"))
                {
                    string authUrl = url.Remove(url.LastIndexOf('/')) + "/IndexMobile";
                    await client.GetStringAsync(authUrl);
                    json = await client.GetStringAsync(url);
                }

                List<Linha> linhas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Linha>>(json);

                foreach (var linha in linhas)
                {
                    //Route route = new Route(linha.LinhaCodigo, linha.LinhaDescricao);
                    Route route = FindRoute(linha.LinhaCodigo);
                    if (App.Instance.FavoriteRoutes.Contains(route.StringDefinition))
                        route.IsFavorite = true;
                    foreach (var sentido in linha.LsRoteiro)
                    {
                        Direction direction = new Direction(route, sentido.Roteiro, sentido.TipoRoteiro);
                        foreach (var ponto in sentido.LsPontoInteresse)
                        {
                            Stop stop = new Stop(ponto.IdPontoInteresse, direction, ponto.Descricao, new GeographicLocation(ponto.LatitudeCkpt, ponto.LongitudeCkpt), ponto.Endereco);
                            if (App.Instance.FavoriteStops.Contains(stop.StringDefinition))
                                stop.IsFavorite = true;
                            foreach (var veiculo in ponto.LsVeiculo)
                            {
                                if (veiculo.TempoRestante <= 60 && veiculo.SentidoReal == sentido.TipoRoteiro)
                                {
                                    DateTime timeOfArrival = DateTime.Now + TimeSpan.FromMinutes(veiculo.TempoRestante);
                                    GeographicLocation busLocation = new GeographicLocation(veiculo.LatitudeCarro, veiculo.LongitudeCarro);
                                    BusPrediction busPrediction = new BusPrediction(timeOfArrival, stop, veiculo.IdentificadorVeiculo, veiculo.FlAdaptadoCadeirante, veiculo.Distancia, busLocation, veiculo.Endereco, veiculo.IdDispositivo);
                                    busPredictions.Add(busPrediction);
                                }
                            }
                        }
                    }
                }
            }

            var busLastUpdateService = new BusLastUpdateService();
            var busLastUpdatesDict = await busLastUpdateService.GetBusLastUpdates();
            foreach (var prediction in busPredictions)
            {
                if (busLastUpdatesDict.ContainsKey(prediction.BusName))
                    prediction.TimeSinceLastUpdate = DateTime.Now - busLastUpdatesDict[prediction.BusName];
                else
                    prediction.TimeSinceLastUpdate = TimeSpan.Zero;
            }

            return busPredictions;
        }

        private Route FindRoute(int routeId)
        {
            foreach (var route in App.Instance.AllRoutes)
                if (route.Id == routeId)
                    return route;
            return null;
        }
    }

    internal class Linha
    {
        public int LinhaCodigo { get; set; }
        public string LinhaDescricao { get; set; }
        public string CdAgrupamento { get; set; }
        public List<Sentido> LsRoteiro { get; set; }
    }

    internal class Sentido
    {
        public int TipoRoteiro { get; set; }
        public string Roteiro { get; set; }
        public List<PontoInteresse> LsPontoInteresse { get; set; }
    }

    internal class PontoInteresse
    {
        public int IdPontoInteresse { get; set; }
        public string Descricao { get; set; }
        public float LatitudeCkpt { get; set; }
        public float LongitudeCkpt { get; set; }
        public string Endereco { get; set; }
        public List<Veiculo> LsVeiculo { get; set; }
    }

    internal class Veiculo
    {
        public int IdDispositivo { get; set; }
        public string IdentificadorVeiculo { get; set; }
        public int TempoRestante { get; set; }
        public bool FlAdaptadoCadeirante { get; set; }
        public bool FlEnvioMensagem { get; set; }
        public string Endereco { get; set; }
        public int Distancia { get; set; }
        public float LatitudeCarro { get; set; }
        public float LongitudeCarro { get; set; }
        public int SentidoReal { get; set; }
    }
}
