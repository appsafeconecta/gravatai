﻿using Gravatai.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Service
{
    public class BusSpeedService : IBusSpeedService
    {
        public async Task<double> GetBusSpeed(BusPrediction busPrediction)
        {
            double busSpeed = 0.0;
            using (var client = new System.Net.Http.HttpClient())
            {
                string authUrl = "http://scgen.com.br/WebServices/Binder/Transoft/AutenticarUsuario?usuario=wsrafael.pontes&senha=r@f@3lp0nt3s";
                var authJson = await client.GetStringAsync(authUrl);
                Authentication auth = Newtonsoft.Json.JsonConvert.DeserializeObject<Authentication>(authJson);
                if (!auth.RetornoOK)
                    return 0.0;

                for (int i = 0; i < 3; i++)
                {
                    string busPositionUrl =
                        "http://scgen.com.br/WebServices/Binder/Transoft/ObterPosicaoVeiculo?guidIdentificacao=" +
                        auth.IdentificacaoLogin +
                        "&idEmpresa=1563&veiculo=" + busPrediction.BusName +
                        "&dataHora=" + (DateTime.Now - TimeSpan.FromSeconds(30 * i)).ToString("yyyy-MM-ddTHH:mm:ss");
                    var posicaoJson = await client.GetStringAsync(busPositionUrl);
                    PosicaoVeiculo posicaoVeiculo = Newtonsoft.Json.JsonConvert.DeserializeObject<PosicaoVeiculo>(posicaoJson);

                    string busSpeedUrl =
                        "http://scgen.com.br/WebServices/Binder/Transoft/ObterVelocidadeVeiculo?guidIdentificacao=" +
                        auth.IdentificacaoLogin +
                        "&idEmpresa=1563&veiculo=" + busPrediction.BusName +
                        "&latitude=" + posicaoVeiculo.Latitude.ToString(CultureInfo.InvariantCulture) +
                        "&longitude=" + posicaoVeiculo.Longitude.ToString(CultureInfo.InvariantCulture);
                    var velocidadeJson = await client.GetStringAsync(busSpeedUrl);
                    VelocidadeVeiculo velocidadeVeiculo = Newtonsoft.Json.JsonConvert.DeserializeObject<VelocidadeVeiculo>(velocidadeJson);
                    busSpeed += velocidadeVeiculo.Velocidade;
                }

                busSpeed = busSpeed / 3;
                //busSpeed = busSpeed * 0.2f;
            }
            return busSpeed;
        }
    }

    internal class VelocidadeVeiculo
    {
        public double Velocidade { get; set; }
    }

    internal class PosicaoVeiculo
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string DataHora { get; set; }
    }
}
