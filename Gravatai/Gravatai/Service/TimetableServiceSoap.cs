﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gravatai.Model;
using System.Net.Http;
using System.Xml;
using System.IO;
using System.Globalization;

namespace Gravatai.Service
{
    public class TimetableServiceSoap : ITimetableService
    {
        public async Task<List<TimetablePrediction>> GetTimetablePredictions()
        {
            List<TimetablePrediction> predictions = new List<TimetablePrediction>();

            Uri uri = new Uri("http://suprimentos.sogil.com.br:3195/wsSogil/soap/INgOpVeiculo");
            using (var client = new HttpClient())
            {
                string authUrl = "http://scgen.com.br/WebServices/Binder/Transoft/AutenticarUsuario?usuario=wsrafael.pontes&senha=r@f@3lp0nt3s";
                var authJson = await client.GetStringAsync(authUrl);
                Authentication auth = Newtonsoft.Json.JsonConvert.DeserializeObject<Authentication>(authJson);
                if (!auth.RetornoOK)
                    return null;
                string allBusesUrl =
                    "http://scgen.com.br/WebServices/Binder/Transoft/ObterVeiculos?guidIdentificacao=" +
                    auth.IdentificacaoLogin +
                    "&idEmpresa=1563";
                var allBusesJson = await client.GetStringAsync(allBusesUrl);
                List<VeiculoTransoft> veiculos = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VeiculoTransoft>>(allBusesJson);
                string allRoutesUrl =
                    "http://scgen.com.br/WebServices/Binder/Transoft/ObterLinhas?guidIdentificacao=" +
                    auth.IdentificacaoLogin +
                    "&idEmpresa=1563";
                var allRoutesJson = await client.GetStringAsync(allRoutesUrl);
                List<LinhaTransoft> linhas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LinhaTransoft>>(allRoutesJson);

                // Temporary: get sigla from id for each route
                foreach (var linha in linhas)
                    linha.Sigla = linha.Id.ToString();
                /*
                foreach (var linha in linhas)
                {
                    string routePointsUrl =
                        "http://scgen.com.br/WebServices/Binder/Transoft/ObterPontosRoteiroViagemLinha?guidIdentificacao=" +
                        auth.IdentificacaoLogin +
                        "&idEmpresa=1563&idLinha=" + linha.Id;
                    var routePointsJson = await client.GetStringAsync(routePointsUrl);
                    List<PontoRoteiroViagemLinha> pontoLinhas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PontoRoteiroViagemLinha>>(routePointsJson);
                    if (pontoLinhas.Count > 0)
                        linha.Sigla = pontoLinhas[0].LinhaSigla;
                    else
                        linha.Sigla = "E" + linha.Id.ToString();
                }
                */

                client.DefaultRequestHeaders.Add("SOAPAction", "urn:NgOpVeiculoIntf-INgOpVeiculo#GetViagem");

                foreach (var veiculo in veiculos)
                {
                    string soapString = ConstructSoapRequest(veiculo.Placa);
                    SoapRetorno soapRetorno = new SoapRetorno();
                    var content = new StringContent(soapString, Encoding.UTF8, "text/xml");
                    using (var response = await client.PostAsync(uri, content))
                    {
                        var soapResponse = await response.Content.ReadAsStringAsync();
                        XmlReader xmlReader = XmlReader.Create(new StringReader(soapResponse));
                        string element = "";
                        while (xmlReader.Read())
                        {
                            switch (xmlReader.NodeType)
                            {
                                case XmlNodeType.Element:
                                    element = xmlReader.Name;
                                    break;
                                case XmlNodeType.Text:
                                    if ("Linha".Equals(element))
                                        soapRetorno.Linha = xmlReader.Value;
                                    if ("Sentido".Equals(element))
                                        soapRetorno.Sentido = xmlReader.Value;
                                    if ("HoraIni".Equals(element))
                                        soapRetorno.HoraIni = xmlReader.Value;
                                    if ("HoraFim".Equals(element))
                                        soapRetorno.HoraFim = xmlReader.Value;
                                    break;
                            }
                        }
                    }
                    System.Diagnostics.Debug.WriteLine(veiculo.Placa + ": " + soapRetorno.Linha + " (" + predictions.Count + ")");
                    LinhaTransoft linhaTransoft = GetLinhaTransoftFromSigla(soapRetorno.Linha, linhas);
                    if (linhaTransoft != null)
                    {
                        Route route = new Route(linhaTransoft.Id, linhaTransoft.Descricao);
                        int directionType = int.Parse(soapRetorno.Sentido);
                        string directionName = directionType == 1 ? "IDA/CIRCULAR" : "VOLTA";
                        Direction direction = new Direction(route, directionName, directionType);
                        DateTime start = DateTime.ParseExact(soapRetorno.HoraIni, "HH:mm", CultureInfo.InvariantCulture);
                        DateTime end = DateTime.ParseExact(soapRetorno.HoraFim, "HH:mm", CultureInfo.InvariantCulture);
                        // Get Stops
                        string routeStopsUrl =
                            "http://scgen.com.br/WebServices/Binder/Transoft/ObterPontosLinha?guidIdentificacao=" +
                            auth.IdentificacaoLogin +
                            "&idEmpresa=1563" +
                            "&idLinha=" + route.Id;
                        var routeStopsJson = await client.GetStringAsync(routeStopsUrl);
                        List<PontoLinhaSoap> pontosLinha = new List<PontoLinhaSoap>();
                        try { pontosLinha = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PontoLinhaSoap>>(routeStopsJson); } catch (Exception ex) { }
                        int maxOrder = GetMaxOrder(directionType, pontosLinha);
                        foreach (var pontoLinha in pontosLinha)
                        {
                            double ratio = (double)(pontoLinha.Ordem - 1) / Math.Max((maxOrder - 1), 1);
                            DateTime timeOfArrival = start + TimeSpan.FromTicks((long)((end - start).Ticks * ratio));
                            Stop stop = new Stop(pontoLinha.Id, direction, pontoLinha.Descricao, new GeographicLocation(pontoLinha.Latitude, pontoLinha.Longitude), "");
                            TimetablePrediction timetablePrediction = new TimetablePrediction(timeOfArrival, stop);
                            predictions.Add(timetablePrediction);
                        }
                    }
                }
            }

            return predictions;
        }

        private string ConstructSoapRequest(string placa)
        {
            string soapmessage = ""
            + "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:NgOpVeiculoIntf-INgOpVeiculo\" > "
            + " <soapenv:Header/>"
            + " <soapenv:Body>"
            + " <urn:GetViagem soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" > "
            + " <Placas xsi:type=\"xsd:string\">" + placa + "</Placas>"
            + " </urn:GetViagem>"
            + " </soapenv:Body>"
            + "</soapenv:Envelope>";
            return soapmessage;
        }

        private LinhaTransoft GetLinhaTransoftFromSigla(string sigla, List<LinhaTransoft> linhas)
        {
            foreach (var linha in linhas)
                if (sigla.Equals(linha.Sigla))
                    return linha;
            return null;
        }

        private int GetMaxOrder(int directionType, List<PontoLinhaSoap> pontosLinha)
        {
            int maxOrder = 0;
            foreach (var pontoLinha in pontosLinha)
                if (pontoLinha.Sentido == directionType)
                    maxOrder++;
            return maxOrder;
        }
    }

    internal class SoapRetorno
    {
        public string Linha { get; set; }
        public string Sentido { get; set; }
        public string HoraIni { get; set; }
        public string HoraFim { get; set; }
    }

    internal class VeiculoTransoft
    {
        public string Id { get; set; }
        public string Descricao { get; set; }
        public string Placa { get; set; }
    }

    internal class LinhaTransoft
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string Sigla { get; set; }
    }

    internal class PontoLinhaSoap
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int IdLinha { get; set; }
        public int Sentido { get; set; }
        public int Ordem { get; set; }
    }
}
