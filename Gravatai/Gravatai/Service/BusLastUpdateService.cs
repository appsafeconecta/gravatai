﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Service
{
    public class BusLastUpdateService : IBusLastUpdateService
    {
        public async Task<Dictionary<string, DateTime>> GetBusLastUpdates()
        {
            Dictionary<string, DateTime> busLastUpdatesDict = new Dictionary<string, DateTime>();
            using (var client = new System.Net.Http.HttpClient())
            {
                string authUrl = "http://scgen.com.br/WebServices/Binder/Transoft/AutenticarUsuario?usuario=wsrafael.pontes&senha=r@f@3lp0nt3s";
                var authJson = await client.GetStringAsync(authUrl);
                Authentication auth = Newtonsoft.Json.JsonConvert.DeserializeObject<Authentication>(authJson);
                if (!auth.RetornoOK)
                    return null;

                string busLastPositionsUrl =
                    "http://scgen.com.br/WebServices/Binder/Transoft/ObterUltimasCoordenadasVeiculos?guidIdentificacao=" +
                    auth.IdentificacaoLogin +
                    "&idEmpresa=1563";
                var json = await client.GetStringAsync(busLastPositionsUrl);
                List<UltimaCoordenadaVeiculo> ultimasCoordenadasVeiculos = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UltimaCoordenadaVeiculo>>(json);
                foreach (var ultimaCoordenada in ultimasCoordenadasVeiculos)
                {
                    bool validLastUpdateTime = DateTime.TryParseExact(ultimaCoordenada.DataHora, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime lastUpdate);
                    if (validLastUpdateTime)
                    {
                        if (!busLastUpdatesDict.ContainsKey(ultimaCoordenada.Veiculo))
                        {
                            busLastUpdatesDict.Add(ultimaCoordenada.Veiculo, lastUpdate);
                        }
                        else
                        {
                            busLastUpdatesDict[ultimaCoordenada.Veiculo] = lastUpdate;
                        }
                    }
                }
            }
            return busLastUpdatesDict;
        }
    }

    internal class UltimaCoordenadaVeiculo
    {
        public string Veiculo { get; set; }
        public string DataHora { get; set; }
    }
}
