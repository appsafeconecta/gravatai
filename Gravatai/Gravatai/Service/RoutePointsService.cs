﻿using Gravatai.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Service
{
    public class RoutePointsService : IRoutePointsService
    {
        public async Task<List<RoutePoint>> GetRoutePoints(int routeId, int directionType)
        {
            List<RoutePoint> routePoints = new List<RoutePoint>();
            using (var client = new System.Net.Http.HttpClient())
            {
                string authUrl = "http://scgen.com.br/WebServices/Binder/Transoft/AutenticarUsuario?usuario=wsrafael.pontes&senha=r@f@3lp0nt3s";
                var authJson = await client.GetStringAsync(authUrl);
                Authentication auth = Newtonsoft.Json.JsonConvert.DeserializeObject<Authentication>(authJson);
                if (!auth.RetornoOK)
                    return null;
                string routePointsUrl =
                    "http://scgen.com.br/WebServices/Binder/Transoft/ObterPontosRoteiroViagemLinha?guidIdentificacao=" +
                    auth.IdentificacaoLogin +
                    "&idEmpresa=1563&idLinha=" + routeId;
                var json = await client.GetStringAsync(routePointsUrl);
                List<PontoRoteiroViagemLinha> pontoLinhas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PontoRoteiroViagemLinha>>(json);

                JArray a = JArray.Parse(json);
                int i = 0;
                foreach (JObject o in a.Children<JObject>())
                {
                    int id = (int)o["$id"];
                    pontoLinhas[i].Id = id;
                    i++;
                }

                foreach (var pontoLinha in pontoLinhas)
                {
                    if (pontoLinha.Sentido == directionType)
                        routePoints.Add(new RoutePoint(pontoLinha.Id, pontoLinha.Latitude, pontoLinha.Longitude));
                }

            }
            return routePoints.OrderBy(o => o.Id).ToList();
        }
    }

    internal class Authentication
    {
        public bool RetornoOK { get; set; }
        public string IdentificacaoLogin { get; set; }
    }

    internal class PontoRoteiroViagemLinha
    {
        public int Id { get; set; }
        public string LinhaSigla { get; set; }
        public int Sentido { get; set; }
        public bool FlSegunda { get; set; }
        public bool FlTerca { get; set; }
        public bool FlQuarta { get; set; }
        public bool FlQuinta { get; set; }
        public bool FlSexta { get; set; }
        public bool FlSabado { get; set; }
        public bool FlDomingo { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}
