﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gravatai.Model;
using System.Net.Http;
using HtmlAgilityPack;

namespace Gravatai.Service
{
    public class TimetableService : ITimetableService
    {
        public async Task<List<TimetablePrediction>> GetTimetablePredictions()
        {
            List<TimetablePrediction> predictions = new List<TimetablePrediction>();
            List<TimeGroup> timeGroups = new List<TimeGroup>();
            using (var client = new HttpClient())
            {
                int dayOfWeek = (int)DateTime.Now.DayOfWeek + 1;
                var jsonLinhas = await client.GetStringAsync("http://www.sogil.com.br/mobile/routines.php?action=findLinhasMunicipal&idCidadeOrigem=4309209&idDia=" + dayOfWeek + "&idLinha=&idVariante=");
                List<LinhaSogil> linhas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LinhaSogil>>(jsonLinhas);
                foreach (var linha in linhas)
                {
                    //System.Diagnostics.Debug.WriteLine(linha.Text);
                    var jsonVariantes = await client.GetStringAsync("http://www.sogil.com.br/mobile/routines.php?action=findVariantesMunicipal&idCidadeOrigem=4309209&idDia=" + dayOfWeek + "&idLinha=" + linha.Value + "&idVariante=");
                    List<VarianteSogil> variantes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VarianteSogil>>(jsonVariantes);
                    foreach (var variante in variantes)
                    {
                        //System.Diagnostics.Debug.WriteLine("\t" + variante.Text);
                        var content = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string,string>("idCidadeOrigem","4309209"),
                            new KeyValuePair<string, string>("idDia", dayOfWeek.ToString()),
                            new KeyValuePair<string, string>("idLinha", linha.Value),
                            new KeyValuePair<string, string>("idVariante", variante.Value),
                            new KeyValuePair<string, string>("voltar", "0"),
                        });
                        var result = await client.PostAsync("http://www.sogil.com.br/mobile/routines.php?action=findHorariosMunicipal", content);
                        string resultContent = await result.Content.ReadAsStringAsync();
                        var htmlDoc = new HtmlDocument();
                        htmlDoc.LoadHtml(resultContent);

                        TimeGroup timeGroup = new TimeGroup(linha.Text, variante.Text);

                        List<HtmlNode> uls = htmlDoc.DocumentNode.Descendants().Where(x => (x.Name == "ul" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("listahorarios municipal"))).ToList();
                        foreach (var ul in uls)
                        {
                            var lis = ul.Descendants("li").ToList();
                            for (int i = 0; i < lis.Count; i++)
                            {
                                var li = lis[i];
                                var spans = li.Descendants("span").ToList();
                                //string roteiro = spans[0].InnerText;
                                //System.Diagnostics.Debug.WriteLine("\t\t" + roteiro);
                                foreach (var span in spans)
                                {
                                    var title = span.GetAttributeValue("title", "");
                                    if (title.Contains("Escala de veículo adaptado sujeita a alteração"))
                                    {
                                        string timeOfDay = span.InnerText;
                                        //System.Diagnostics.Debug.WriteLine("\t\t\t" + timeOfDay);
                                        if (!timeGroup.Times.ContainsKey(i + 1))
                                            timeGroup.Times.Add(i + 1, new List<string>());
                                        timeGroup.Times[i + 1].Add(timeOfDay);
                                    }
                                    var em = span.Descendants("em").ToList();
                                    if (em.Count > 0)
                                    {
                                        string duration = em[0].InnerText;
                                        //System.Diagnostics.Debug.WriteLine("\t\t" + duration);
                                        timeGroup.Duration = duration;
                                    }
                                }
                            }
                        }

                        timeGroups.Add(timeGroup);
                        // System.Diagnostics.Debug.WriteLine(resultContent);
                    }
                }
            }


            return predictions;
        }
    }

    internal class LinhaSogil
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }

    internal class VarianteSogil
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }

    internal class TimeGroup
    {
        public string RouteName { get; private set; }
        public string VariantName { get; private set; }
        public Dictionary<int, List<string>> Times { get; private set; }
        public string Duration { get; set; }

        public TimeGroup(string routeName, string variantName)
        {
            RouteName = routeName;
            VariantName = variantName;
            Times = new Dictionary<int, List<string>>();
        }
    }
}
