﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gravatai.Model;

namespace Gravatai.Service
{
    public class AllRoutesService : IAllRoutesService
    {
        public async Task<List<Route>> GetAllRoutes()
        {
            DateTime start = DateTime.UtcNow;

            List<Route> routes = new List<Route>();
            using (var client = new System.Net.Http.HttpClient())
            {
                string authUrl = "http://scgen.com.br/WebServices/Binder/Transoft/AutenticarUsuario?usuario=wsrafael.pontes&senha=r@f@3lp0nt3s";
                var authJson = await client.GetStringAsync(authUrl);
                Authentication auth = Newtonsoft.Json.JsonConvert.DeserializeObject<Authentication>(authJson);
                if (!auth.RetornoOK)
                    return null;
                string allRoutesUrl =
                    "http://scgen.com.br/WebServices/Binder/Transoft/ObterLinhas?guidIdentificacao=" +
                    auth.IdentificacaoLogin +
                    "&idEmpresa=1563";
                var json = await client.GetStringAsync(allRoutesUrl);
                List<LinhaBase> linhas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LinhaBase>>(json);

                foreach (var linha in linhas)
                {
                    routes.Add(new Route(linha.Id, linha.Descricao.Trim()));
                }

                DateTime middle = DateTime.UtcNow;
                
                for (int i = 1; i <= 7; i++)
                {
                    string weekdayUrl =
                        "http://scgen.com.br/Webservices/Binder/Transoft/RetornaLinhasDiaSemana?guidIdentificacao=" +
                        auth.IdentificacaoLogin +
                        "&idEmpresa=1563" +
                        "&diaDaSemana=" + i;
                    var weekdayJson = await client.GetStringAsync(weekdayUrl);
                    List<LinhaBase> linhasWeekday = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LinhaBase>>(weekdayJson);
                    foreach (var linhaWeekday in linhasWeekday)
                    {
                        foreach (var route in routes)
                        {
                            if (route.Id == linhaWeekday.Id)
                            {
                                route.Weekdays.Add(i - 1);
                            }
                        }
                    }
                }

                DateTime end = DateTime.UtcNow;

                System.Diagnostics.Debug.WriteLine((middle - start).Milliseconds);
                System.Diagnostics.Debug.WriteLine((end - middle).Milliseconds);

            }

            return routes;
        }
    }

    internal class LinhaBase
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}
