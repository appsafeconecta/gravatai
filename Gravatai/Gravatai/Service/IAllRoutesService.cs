﻿using Gravatai.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Service
{
    public interface IAllRoutesService
    {
        Task<List<Route>> GetAllRoutes();
    }
}
