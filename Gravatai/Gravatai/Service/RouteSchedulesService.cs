﻿using Gravatai.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Service
{
    public class RouteSchedulesService : IRouteSchedulesService
    {
        public async Task<List<RouteSchedule>> GetRouteSchedules(int routeId, int directionType, int weekday)
        {
            List<RouteSchedule> routeSchedules = new List<RouteSchedule>();
            using (var client = new System.Net.Http.HttpClient())
            {
                string authUrl = "http://scgen.com.br/WebServices/Binder/Transoft/AutenticarUsuario?usuario=wsrafael.pontes&senha=r@f@3lp0nt3s";
                var authJson = await client.GetStringAsync(authUrl);
                Authentication auth = Newtonsoft.Json.JsonConvert.DeserializeObject<Authentication>(authJson);
                if (!auth.RetornoOK)
                    return null;
                int today = (int)DateTime.Today.DayOfWeek;
                int dayDiff = (today - weekday - 1 + 7) % 7 + 1;
                DateTime queryDate = DateTime.Today.AddDays(-dayDiff);
                string queryDateStr = queryDate.ToString("yyyy-MM-dd");
                System.Diagnostics.Debug.WriteLine("querying for " + queryDateStr + " (" + weekday + ")");
                string routeSchedulesUrl =
                    "http://scgen.com.br/WebServices/Binder/Transoft/ObterProgramacaoViagensAnteriores?guidIdentificacao=" +
                    auth.IdentificacaoLogin +
                    "&idEmpresa=1563&idLinha=" + routeId + "&sentido=" + directionType + "&data=" + queryDateStr;
                System.Diagnostics.Debug.WriteLine(routeSchedulesUrl);
                var json = await client.GetStringAsync(routeSchedulesUrl);

                string errorString = "{\"RetornoOK\":false,\"IdentificacaoLogin\":null,\"DescricaoErro\":\"Index and length must refer to a location within the string.\\r\\nParameter name: length\",\"Empresas\":null}";
                if (string.Equals(errorString, json))
                    json = "{}";
                if (string.Equals("{}", json))
                    return new List<RouteSchedule>();
                List<Programacao> programacoes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Programacao>>(json);

                foreach (var programacao in programacoes)
                {
                    routeSchedules.Add(new RouteSchedule(programacao.IP, programacao.FP));
                }

            }
            return routeSchedules;
        }
    }

    internal class Programacao
    {
        public string IP { get; set; }
        public string FP { get; set; }
    }
}
