﻿using Gravatai.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DirectionsPage : ContentPage
    {
        private Route route;

        public DirectionsPage(Route route)
        {
            InitializeComponent();

            Title = route.Name;

            this.route = route;
            ListView.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is Direction selectedDirection)
            {
                ListView.SelectedItem = null;
                Navigation.PushAsync(new StopsPage(selectedDirection));
            }
        }

        private void UpdateListView()
        {
            var directions = new List<Direction>();
            foreach (var prediction in App.Instance.Predictions)
            {
                if (prediction.Stop.Direction.Route.Equals(route) && !directions.Contains(prediction.Stop.Direction))
                    directions.Add(prediction.Stop.Direction);
            }
            ListView.ItemsSource = directions;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            UpdateListView();

            //await App.Instance.UpdateBusPredictions();
            //UpdateListView();
        }
    }
}