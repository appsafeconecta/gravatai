﻿using Gravatai.Model;
using Gravatai.Model.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactPage : ContentPage
    {
        public ContactPage()
        {
            InitializeComponent();

            Title = "Fale Conosco";

            ButtonSend.Clicked += ButtonSend_Clicked;
            EditorMessage.Keyboard = Keyboard.Create(KeyboardFlags.CapitalizeSentence);
            EntryName.Keyboard = Keyboard.Create(KeyboardFlags.CapitalizeSentence);
            LoadingWheel.GestureRecognizers.Add(new TapGestureRecognizer());
            List<Route> routes = new List<Route>();
            foreach (var prediction in App.Instance.Predictions)
                if (!routes.Contains(prediction.Stop.Direction.Route))
                    routes.Add(prediction.Stop.Direction.Route);
            PickerRoute.Items.Clear();
            foreach (var route in routes)
                PickerRoute.Items.Add(route.Name);
            TimePicker.Time = DateTime.Now.TimeOfDay;
        }

        private async void ButtonSend_Clicked(object sender, EventArgs e)
        {
            string name = EntryName.IsVisible ? EntryName.Text : "";
            string email = EntryEmail.IsVisible ? EntryEmail.Text : "";
            string telephone = EntryTelephone.IsVisible ? EntryTelephone.Text : "";
            bool wantsResponse = LayoutResponse.IsVisible ? SwitchResponse.IsToggled : false;
            string bus = EntryBus.Text ?? "";
            int selectedRouteIndex = PickerRoute.SelectedIndex;
            string route = selectedRouteIndex != -1 ? PickerRoute.Items[selectedRouteIndex] : "";
            DateTime date = DatePicker.Date + TimePicker.Time;
            string message = EditorMessage.Text ?? "";

            if (wantsResponse && (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(email)))
            {
                await DisplayAlert("Erro", "Para obter um retorno você deve fornecer o seu nome e e-mail.", "OK");
            }
            else
            {
                if (PickerType.SelectedIndex > -1 && PickerSubject.SelectedIndex > -1)
                {
                    string type = PickerType.Items[PickerType.SelectedIndex];
                    string subject = PickerSubject.Items[PickerSubject.SelectedIndex];
                    var result = await DisplayAlert("Enviar", "Deseja enviar a ocorrência?", "Sim", "Cancelar");
                    if (result)
                    {
                        // TODO chamada para WebService
                        LoadingWheel.IsVisible = true;
                        Exception error = null;
                        try
                        {
                            IMailSender mailSender = DependencyService.Get<IMailSender>();
                            await mailSender.Send(name, email, type, subject, message, bus, route, telephone, wantsResponse, date);
                        }
                        catch (Exception ex)
                        {
                            error = ex;
                        }
                        finally
                        {
                            LoadingWheel.IsVisible = false;
                            if (error != null)
                            {
                                await DisplayAlert("Erro", "Não foi possível enviar a ocorrência. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                            }
                            else
                            {
                                await DisplayAlert("Sucesso", "Ocorrência enviada com sucesso.", "OK");
                                try
                                {
                                    await Navigation.PopAsync();
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                                }
                            }
                        }
                    }
                }
                else
                {
                    await DisplayAlert("Erro", "Você deve preencher o tipo e o assunto da mensagem!", "OK");
                }
            }
        }

        private void Anonymous_Toggled(object sender, ToggledEventArgs e)
        {
            EntryName.IsVisible = !e.Value;
            EntryEmail.IsVisible = !e.Value;
            EntryTelephone.IsVisible = !e.Value;
            LayoutResponse.IsVisible = !e.Value;
        }
    }
}