﻿using Gravatai.Model;
using Gravatai.Model.DeviceLocation;
using Gravatai.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NearbyStopsPage : ContentPage
    {
        private ILocationTracker locationTracker;

        public ObservableCollection<StopGroup> StopGroups { get; set; }

        public NearbyStopsPage()
        {
            InitializeComponent();

            Title = "Pontos Próximos";

            locationTracker = DependencyService.Get<ILocationTracker>(DependencyFetchTarget.NewInstance);
            StopGroups = new ObservableCollection<StopGroup>();
            ListView.ItemSelected += ListView_ItemSelected;
            ListView.BindingContext = this;
            LoadingWheel.GestureRecognizers.Add(new TapGestureRecognizer());
        }

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is Stop selectedStop)
            {
                ListView.SelectedItem = null;
                await OpenPredictions(selectedStop);
            }
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            var filter = e.NewTextValue.ToLowerInvariant();
            FilterStops(filter, RadiusSlider.Value, false);
        }

        private void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            var radius = Math.Round(e.NewValue);
            RadiusSlider.Value = radius;
            RadiusLabel.Text = "Busca: " + radius.ToString("0") + "km";
            if (StopGroups != null)
                FilterStops(SearchBar.Text, radius, false);
        }

        private void FilterStops(string filter, double radius, bool forceRedraw)
        {
            if (App.Instance.UserLocation != null)
            {
                if (!string.IsNullOrEmpty(filter))
                    filter = filter.ToLowerInvariant();
                var filteredStops = new List<Stop>();
                foreach (var prediction in App.Instance.Predictions)
                {
                    double distanceFromUser = Formulas.GetKilometersBetween(prediction.Stop.Location, App.Instance.UserLocation.Value);
                    prediction.Stop.DistanceFromUser = distanceFromUser;
                    if (distanceFromUser < radius && !filteredStops.Contains(prediction.Stop))
                    {
                        var lowerStopName = prediction.Stop.Name.ToLowerInvariant();
                        var lowerDirectionName = prediction.Stop.Direction.FullName.ToLowerInvariant();
                        if (string.IsNullOrEmpty(filter) || lowerStopName.Contains(filter) || lowerDirectionName.Contains(filter))
                            filteredStops.Add(prediction.Stop);
                    }
                }

                filteredStops = filteredStops.OrderBy(o => o.DistanceFromUser).ToList();

                bool redraw = false;
                int stopsCount = 0;
                foreach (var stopGroup in StopGroups)
                    stopsCount += stopGroup.Count;
                if (stopsCount != filteredStops.Count)
                    redraw = true;
                if (redraw || forceRedraw)
                {
                    StopGroups.Clear();
                    foreach (var stop in filteredStops)
                    {
                        StopGroup stopGroup = StopGroups.ToList().Find(x => x.Direction.Equals(stop.Direction));
                        if (stopGroup == null)
                        {
                            stopGroup = new StopGroup(stop.Direction);
                            StopGroups.Add(stopGroup);
                        }
                        stopGroup.Add(stop);
                    }
                }
            }
        }

        private async Task OpenPredictions(Stop stop)
        {
            Exception error = null;
            List<RouteSchedule> routeSchedules = null;
            double stopInterpolation = 0;
            try
            {
                LoadingWheel.IsVisible = true;
                int routeId = stop.Direction.Route.Id;
                int directionType = stop.Direction.Type;
                int weekday = (int)DateTime.Today.DayOfWeek;
                Tuple<int, int, int> directionTuple = new Tuple<int, int, int>(routeId, directionType, weekday);
                if (App.Instance.RouteSchedulesDict.ContainsKey(directionTuple))
                {
                    routeSchedules = App.Instance.RouteSchedulesDict[directionTuple];
                }
                else
                {
                    var routeSchedulesService = new RouteSchedulesService();
                    var items = await routeSchedulesService.GetRouteSchedules(routeId, directionType, weekday);
                    routeSchedules = new List<RouteSchedule>();
                    foreach (var routeSchedule in items)
                        routeSchedules.Add(routeSchedule);
                    App.Instance.RouteSchedulesDict.Add(directionTuple, routeSchedules);
                }
                List<RouteStop> routeStops;
                if (App.Instance.RouteStopsDict.ContainsKey(routeId))
                {
                    routeStops = App.Instance.RouteStopsDict[routeId];
                }
                else
                {
                    var routeStopsService = new RouteStopsService();
                    var items = await routeStopsService.GetRouteStops(routeId);
                    routeStops = new List<RouteStop>();
                    foreach (var routeStop in items)
                        routeStops.Add(routeStop);
                    App.Instance.RouteStopsDict.Add(routeId, routeStops);
                }
                int maxOrder = 0;
                int thisOrder = -1;
                foreach (var routeStop in routeStops)
                    if (routeStop.DirectionType == directionType)
                    {
                        maxOrder++;
                        if (routeStop.Id == stop.Id)
                            thisOrder = routeStop.Order;
                    }
                stopInterpolation = (double)(thisOrder - 1) / Math.Max((maxOrder - 1), 1);
                LoadingWheel.IsVisible = false;
            }
            catch (Exception ex)
            {
                error = ex;
            }
            if (error != null)
            {
                await DisplayAlert("Erro", "Não foi possível retornar as previsões do ponto escolhido. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                LoadingWheel.IsVisible = false;
            }
            else
            {
                await Navigation.PushAsync(new PredictionsPage(stop, routeSchedules, stopInterpolation));
            }
        }

        void OnLocationTrackerLocationChanged(object sender, GeographicLocation args)
        {
            locationTracker.PauseTracking();
            LoadingWheel.IsVisible = false;
            TrackingLocationLabel.IsVisible = false;
            App.Instance.UserLocation = args;
            /*
            if (App.Instance.Predictions.Count == 0)
            {
                LoadingWheel.IsVisible = true;
                await App.Instance.UpdateBusPredictions();
                LoadingWheel.IsVisible = false;
            }
            */
            FilterStops(SearchBar.Text, RadiusSlider.Value, true);

            //await App.Instance.UpdateBusPredictions();
            //FilterStops(SearchBar.Text, RadiusSlider.Value, true);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            locationTracker.LocationChanged += OnLocationTrackerLocationChanged;
            locationTracker.StartTracking();
            TrackingLocationLabel.IsVisible = true;
            if (App.Instance.UserLocation == null)
                LoadingWheel.IsVisible = true;
            else
                FilterStops(SearchBar.Text, RadiusSlider.Value, true);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            locationTracker.PauseTracking();
            locationTracker.LocationChanged -= OnLocationTrackerLocationChanged;
        }

        private async void OnOptions(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            Stop stop = (Stop)menuItem.CommandParameter;
            string stopDefinition = stop.StringDefinition;
            if (App.Instance.FavoriteStops.Contains(stopDefinition))
            {
                var result = await DisplayActionSheet("Opções", "Cancelar", null, new string[] { "Desfavoritar" });
                if ("Desfavoritar".Equals(result))
                {
                    App.Instance.RemoveFavoriteStop(stopDefinition);
                }
            }
            else
            {
                var result = await DisplayActionSheet("Opções", "Cancelar", null, new string[] { "Favoritar" });
                if ("Favoritar".Equals(result))
                {
                    App.Instance.SaveStopAsFavorite(stopDefinition);
                }
            }
        }
    }

    public class StopGroup : ObservableCollection<Stop>
    {
        public Direction Direction { get; private set; }

        public StopGroup(Direction direction)
        {
            Direction = direction;
        }
    }
}