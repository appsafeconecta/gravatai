﻿using Gravatai.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DirectionsToUsePage : ContentPage
    {
        public ObservableCollection<DirectionToUse> DirectionsToUse { get; set; }

        public DirectionsToUsePage(List<DirectionToUse> directionsToUse)
        {
            InitializeComponent();

            Title = "Trajetos";

            for (int i = directionsToUse.Count - 1; i >= 0; i--)
                if (directionsToUse[i].OriginDistance > 1.5 || directionsToUse[i].DestinyDistance > 1.5)
                    directionsToUse.RemoveAt(i);
            directionsToUse = directionsToUse.OrderBy(o => (o.OriginDistance + o.DestinyDistance)).ToList();
            DirectionsToUse = new ObservableCollection<DirectionToUse>(directionsToUse);
            LabelNoDirectionsToUse.IsVisible = DirectionsToUse.Count == 0;
            ListView.ItemSelected += ListView_ItemSelected;
            ListView.BindingContext = this;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is DirectionToUse selectedDirectionToUse)
            {
                ListView.SelectedItem = null;
            }
        }
    }
}