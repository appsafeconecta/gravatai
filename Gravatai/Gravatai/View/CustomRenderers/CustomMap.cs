﻿using Gravatai.Model;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Gravatai.View.CustomRenderers
{
    public class CustomMap : Map
    {
        public static readonly BindableProperty BusPinsProperty = BindableProperty.Create(nameof(BusPins), typeof(List<CustomPin>), typeof(CustomMap), new List<CustomPin>(), BindingMode.TwoWay);

        public List<CustomPin> StopPins { get; set; }
        public List<Position> RouteCoordinates { get; set; }
        public List<CustomPin> BusPins
        {
            get { return (List<CustomPin>)GetValue(BusPinsProperty); }
            set { SetValue(BusPinsProperty, value); }
        }

        public delegate void InfoWindowClickDelegate(GeographicLocation location);
        public event InfoWindowClickDelegate InfoWindowClickEvent;

        public CustomMap()
        {
            RouteCoordinates = new List<Position>();
            BusPins = new List<CustomPin>();
        }

        public void OnInfoWindowClick(GeographicLocation location)
        {
            InfoWindowClickEvent?.Invoke(location);
        }
    }
}
