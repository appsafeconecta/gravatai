﻿using Gravatai.Model;
using Gravatai.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PredictionsPage : ContentPage
    {
        private Stop stop;
        private bool keepPolling;
        private bool firstUpdate;

        public ObservableCollection<Prediction> Predictions { get; set; }
        public ObservableCollection<RouteScheduleGroup> Schedules { get; set; }

        public PredictionsPage(Stop stop, List<RouteSchedule> routeSchedules, double stopInterpolation)
        {
            InitializeComponent();

            Title = stop.Name;

            this.stop = stop;

            Predictions = new ObservableCollection<Prediction>();
            PredictionsListView.ItemSelected += PredictionsListView_ItemSelected;
            PredictionsListView.BindingContext = this;

            List<DateTime> nextTimes = new List<DateTime>();
            foreach (var routeSchedule in routeSchedules)
            {
                DateTime startTime = DateTime.ParseExact(routeSchedule.Start, "HH:mm", CultureInfo.InvariantCulture);
                DateTime finishTime = DateTime.ParseExact(routeSchedule.Finish, "HH:mm", CultureInfo.InvariantCulture);
                DateTime stopTime = startTime + TimeSpan.FromTicks((long)((finishTime - startTime).Ticks * stopInterpolation));
                if (stopTime > DateTime.Now - TimeSpan.FromHours(1))
                    nextTimes.Add(stopTime);
            }

            Schedules = new ObservableCollection<RouteScheduleGroup>();
            for (int i = 0; i < nextTimes.Count; i += 4)
            {
                string s1 = nextTimes.Count > i + 0 && stopInterpolation >= 0 ? nextTimes[i + 0].ToString("HH:mm", CultureInfo.InvariantCulture) : "";
                string s2 = nextTimes.Count > i + 1 && stopInterpolation >= 0 ? nextTimes[i + 1].ToString("HH:mm", CultureInfo.InvariantCulture) : "";
                string s3 = nextTimes.Count > i + 2 && stopInterpolation >= 0 ? nextTimes[i + 2].ToString("HH:mm", CultureInfo.InvariantCulture) : "";
                string s4 = nextTimes.Count > i + 3 && stopInterpolation >= 0 ? nextTimes[i + 3].ToString("HH:mm", CultureInfo.InvariantCulture) : "";
                RouteScheduleGroup group = new RouteScheduleGroup(s1, s2, s3, s4);
                Schedules.Add(group);
            }
            SchedulesListView.ItemSelected += SchedulesListView_ItemSelected;
            SchedulesListView.BindingContext = this;
        }

        private async void PredictionsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is Prediction selectedPrediction)
            {
                PredictionsListView.SelectedItem = null;
                if (selectedPrediction is BusPrediction selectedBusPrediction)
                {
                    await OpenMap(selectedBusPrediction);
                }
            }
        }

        private void SchedulesListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is RouteScheduleGroup selectedGroup)
            {
                SchedulesListView.SelectedItem = null;
            }
        }

        private void UpdateListView()
        {
            for (int i = Predictions.Count - 1; i >= 0; i--)
            {
                if (Predictions[i] is BusPrediction busPrediction)
                {
                    var newBusPrediction = GetNewBusPrediction(busPrediction);
                    if (newBusPrediction == null)
                    {
                        Predictions.RemoveAt(i);
                    }
                    else
                    {
                        Predictions[i].TimeOfArrival = newBusPrediction.TimeOfArrival;
                    }
                }
            }
            foreach (var prediction in App.Instance.Predictions)
            {
                if (prediction is BusPrediction busPrediction)
                {
                    if (busPrediction.Stop.Equals(stop) && !Predictions.Contains(busPrediction))
                    {
                        Predictions.Add(busPrediction);
                    }
                }
            }
        }

        private Prediction GetNewBusPrediction(BusPrediction oldBusPrediction)
        {
            foreach (var prediction in App.Instance.Predictions)
            {
                if (prediction is BusPrediction busPrediction)
                {
                    if (busPrediction.Equals(oldBusPrediction))
                        return busPrediction;
                }
            }
            return null;
        }

        private async Task ContinuousWebRequest()
        {
            while (keepPolling)
            {
                if (firstUpdate)
                {
                    firstUpdate = false;
                    LoadingWheel.IsVisible = true;
                }
                await App.Instance.UpdateBusPredictions();
                UpdateListView();
                LoadingWheel.IsVisible = false;
                if (keepPolling)
                {
                    await Task.Delay(TimeSpan.FromSeconds(30));
                }
            }
        }

        private async Task OpenMap(BusPrediction busPrediction)
        {
            Exception error = null;
            List<RoutePoint> routePoints = null;
            double busSpeed = 0;
            try
            {
                LoadingWheel.IsVisible = true;
                int routeId = busPrediction.Stop.Direction.Route.Id;
                int directionType = busPrediction.Stop.Direction.Type;
                Tuple<int, int> directionTuple = new Tuple<int, int>(routeId, directionType);
                if (App.Instance.RoutePointsDict.ContainsKey(directionTuple))
                {
                    routePoints = App.Instance.RoutePointsDict[directionTuple];
                }
                else
                {
                    var routePointsService = new RoutePointsService();
                    var items = await routePointsService.GetRoutePoints(routeId, directionType);
                    routePoints = new List<RoutePoint>();
                    foreach (var routePoint in items)
                        routePoints.Add(routePoint);
                    App.Instance.RoutePointsDict.Add(directionTuple, routePoints);
                }
                var busSpeedService = new BusSpeedService();
                busSpeed = await busSpeedService.GetBusSpeed(busPrediction);
                LoadingWheel.IsVisible = false;
            }
            catch (Exception ex)
            {
                error = ex;
            }
            if (error != null)
            {
                await DisplayAlert("Erro", "Não foi possível retornar o trajeto do ônibus escolhido. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                LoadingWheel.IsVisible = false;
            }
            else
            {
                Direction direction = busPrediction.Stop.Direction;
                List<Stop> stops = new List<Stop>();
                foreach (var prediction in App.Instance.Predictions)
                    if (prediction.Stop.Direction.Equals(busPrediction.Stop.Direction))
                        stops.Add(prediction.Stop);
                await Navigation.PushAsync(new MapPage(busPrediction, stops, routePoints, busSpeed, this.stop));
                //await Navigation.PushModalAsync(new MapPage(busPrediction, stops, routePoints));
            }
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            keepPolling = true;
            firstUpdate = true;
            await ContinuousWebRequest();
        }

        protected override void OnDisappearing()
        {
            keepPolling = false;
            base.OnDisappearing();
        }
    }
}