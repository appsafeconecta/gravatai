﻿using Gravatai.Model;
using Gravatai.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FavoriteStopsPage : ContentPage
    {
        public FavoriteStopsPage()
        {
            InitializeComponent();

            ListView.ItemSelected += ListView_ItemSelected;
            LoadingWheel.GestureRecognizers.Add(new TapGestureRecognizer());
        }

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is FavoriteStop favoriteStop)
            {
                ListView.SelectedItem = null;
                if (favoriteStop.Stop != null)
                    await OpenPredictions(favoriteStop.Stop);
            }
        }

        private void UpdateListView()
        {
            var favoriteStops = new List<FavoriteStop>();
            foreach (var favStopStr in App.Instance.FavoriteStops)
            {
                var favoriteStop = new FavoriteStop(favStopStr);
                foreach (var prediction in App.Instance.Predictions)
                {
                    string def = prediction.Stop.StringDefinition;
                    if (def.Equals(favoriteStop.StringDefinition))
                    {
                        favoriteStop.Stop = prediction.Stop;
                        break;
                    }
                }
                favoriteStops.Add(favoriteStop);
            }
            ListView.ItemsSource = favoriteStops;
            LabelNoFavoriteStops.IsVisible = (favoriteStops.Count == 0);
        }

        private async Task OpenPredictions(Stop stop)
        {
            Exception error = null;
            List<RouteSchedule> routeSchedules = null;
            double stopInterpolation = 0;
            try
            {
                LoadingWheel.IsVisible = true;
                int routeId = stop.Direction.Route.Id;
                int directionType = stop.Direction.Type;
                int weekday = (int)DateTime.Today.DayOfWeek;
                Tuple < int, int, int> directionTuple = new Tuple<int, int, int>(routeId, directionType, weekday);
                if (App.Instance.RouteSchedulesDict.ContainsKey(directionTuple))
                {
                    routeSchedules = App.Instance.RouteSchedulesDict[directionTuple];
                }
                else
                {
                    var routeSchedulesService = new RouteSchedulesService();
                    var items = await routeSchedulesService.GetRouteSchedules(routeId, directionType, weekday);
                    routeSchedules = new List<RouteSchedule>();
                    foreach (var routeSchedule in items)
                        routeSchedules.Add(routeSchedule);
                    App.Instance.RouteSchedulesDict.Add(directionTuple, routeSchedules);
                }
                List<RouteStop> routeStops;
                if (App.Instance.RouteStopsDict.ContainsKey(routeId))
                {
                    routeStops = App.Instance.RouteStopsDict[routeId];
                }
                else
                {
                    var routeStopsService = new RouteStopsService();
                    var items = await routeStopsService.GetRouteStops(routeId);
                    routeStops = new List<RouteStop>();
                    foreach (var routeStop in items)
                        routeStops.Add(routeStop);
                    App.Instance.RouteStopsDict.Add(routeId, routeStops);
                }
                int maxOrder = 0;
                int thisOrder = -1;
                foreach (var routeStop in routeStops)
                    if (routeStop.DirectionType == directionType)
                    {
                        maxOrder++;
                        if (routeStop.Id == stop.Id)
                            thisOrder = routeStop.Order;
                    }
                stopInterpolation = (double)(thisOrder - 1) / Math.Max((maxOrder - 1), 1);
                LoadingWheel.IsVisible = false;
            }
            catch (Exception ex)
            {
                error = ex;
            }
            if (error != null)
            {
                await DisplayAlert("Erro", "Não foi possível retornar as previsões do ponto escolhido. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                LoadingWheel.IsVisible = false;
            }
            else
            {
                await Navigation.PushAsync(new PredictionsPage(stop, routeSchedules, stopInterpolation));
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            UpdateListView();
        }

        private void OnUnfavorite(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            FavoriteStop favoriteStop = (FavoriteStop)menuItem.CommandParameter;
            string stopDefinition = favoriteStop.StringDefinition;
            if (App.Instance.FavoriteStops.Contains(stopDefinition))
            {
                App.Instance.RemoveFavoriteStop(stopDefinition);
                UpdateListView();
            }
        }

        private async void OnViewRoute(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            FavoriteStop favoriteStop = (FavoriteStop)menuItem.CommandParameter;
            await DisplayAlert("Linha", favoriteStop.RouteName, "OK");
        }

        protected override bool OnBackButtonPressed()
        {
            Application.Current.MainPage = new MainPage();
            return true;
        }
    }
}