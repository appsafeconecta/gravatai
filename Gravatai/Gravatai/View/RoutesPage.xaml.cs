﻿using Gravatai.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RoutesPage : ContentPage
    {
        public ObservableCollection<Route> Routes { get; set; }

        public RoutesPage()
        {
            InitializeComponent();

            Routes = new ObservableCollection<Route>();
            ListView.ItemSelected += ListView_ItemSelected;
            ListView.BindingContext = this;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is Route selectedRoute)
            {
                ListView.SelectedItem = null;
                Navigation.PushAsync(new DirectionsPage(selectedRoute));
            }
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            var filter = e.NewTextValue.ToLowerInvariant();
            FilterRoutes(filter, false);
        }

        private void FilterRoutes(string filter, bool forceRedraw)
        {
            if (!string.IsNullOrEmpty(filter))
                filter = filter.ToLowerInvariant();
            var filteredRoutes = new List<Route>();
            foreach (var prediction in App.Instance.Predictions)
                if (!filteredRoutes.Contains(prediction.Stop.Direction.Route))
                {
                    var lowerRouteName = prediction.Stop.Direction.Route.Name.ToLowerInvariant();
                    if (string.IsNullOrEmpty(filter) || lowerRouteName.Contains(filter))
                        filteredRoutes.Add(prediction.Stop.Direction.Route);
                }

            bool redraw = false;
            foreach (var route in Routes)
                if (!filteredRoutes.Contains(route))
                    redraw = true;
            foreach (var route in filteredRoutes)
                if (!Routes.Contains(route))
                    redraw = true;
            if (redraw || forceRedraw)
            {
                Routes.Clear();
                foreach (var route in filteredRoutes)
                    Routes.Add(route);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            /*
            if (App.Instance.Predictions.Count == 0)
            {
                LoadingWheel.IsVisible = true;
                await App.Instance.UpdateBusPredictions();
                LoadingWheel.IsVisible = false;
            }
            */
            FilterRoutes(SearchBar.Text, false);

            //await App.Instance.UpdateBusPredictions();
            //FilterRoutes(SearchBar.Text, true);
        }

        private async void OnOptions(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            Route route = (Route)menuItem.CommandParameter;
            string routeDefinition = route.StringDefinition;
            if (App.Instance.FavoriteRoutes.Contains(routeDefinition))
            {
                var result = await DisplayActionSheet("Opções", "Cancelar", null, new string[] { "Desfavoritar" });
                if ("Desfavoritar".Equals(result))
                {
                    App.Instance.RemoveFavoriteRoute(routeDefinition);
                }
            }
            else
            {
                var result = await DisplayActionSheet("Opções", "Cancelar", null, new string[] { "Favoritar" });
                if ("Favoritar".Equals(result))
                {
                    App.Instance.SaveRouteAsFavorite(routeDefinition);
                }
            }
        }
    }
}