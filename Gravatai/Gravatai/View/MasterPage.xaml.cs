﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : ContentPage
    {
        public ListView MenuOptions { get { return menuOptions; } }

        public MasterPage()
        {
            InitializeComponent();

            var masterPageItems = new List<MasterPageItem>();
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Menu Principal",
                IconSource = "home",
                TargetType = typeof(HomePage)
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Pontos Favoritos",
                IconSource = "star",
                TargetType = typeof(FavoriteStopsPage)
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Linhas Favoritas",
                IconSource = "send",
                TargetType = typeof(FavoriteRoutesPage)
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Indique a um amigo",
                IconSource = "share",
                TargetType = typeof(ComingSoonPage)
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Ajuda",
                IconSource = "info",
                TargetType = typeof(ComingSoonPage)
            });

            menuOptions.ItemsSource = masterPageItems;
        }
    }
}