﻿using Gravatai.Model;
using Gravatai.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchMapPage : ContentPage
    {
        private List<Place> placesList;
        private GeographicLocation? originLocation;
        private GeographicLocation? destinyLocation;

        public SearchMapPage()
        {
            InitializeComponent();

            Title = "Como Chegar";

            ButtonOrigin.Clicked += ButtonOrigin_Clicked;
            ButtonDestiny.Clicked += ButtonDestiny_Clicked;

            ButtonOrigin.IsEnabled = false;
            ButtonDestiny.IsEnabled = false;
            EntryDestiny.IsEnabled = false;

            map.IsShowingUser = false;
            map.BusPins = new List<CustomPin>();
            map.StopPins = new List<CustomPin>();
            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(-29.950471, -50.983270), Distance.FromMeters(5000)));
            map.InfoWindowClickEvent += Map_OnInfoWindowClick;

            placesList = new List<Place>();

            EntryOrigin.Placeholder = "Digite a sua Origem (Endereço, Shopping, Local, etc...)";
            EntryDestiny.Placeholder = "";
        }

        private void Map_OnInfoWindowClick(GeographicLocation location)
        {
            System.Diagnostics.Debug.WriteLine(location.Latitude + ", " + location.Longitude);
            foreach (var place in placesList)
            {
                if (place.Location.Equals(location))
                {
                    placesList.Clear();
                    map.StopPins.Clear();
                    map.Pins.Clear();
                    if (originLocation == null)
                    {
                        EntryOrigin.Text = place.Name;
                        originLocation = place.Location;
                        ButtonOrigin.IsEnabled = false;
                        EntryOrigin.IsEnabled = false;
                        EntryDestiny.IsEnabled = true;
                        EntryDestiny.Placeholder = "Digite o seu Destino (Endereço, Shopping, Local, etc...)";
                    }
                    else
                    {
                        EntryDestiny.Text = place.Name;
                        destinyLocation = place.Location;
                        ButtonDestiny.IsEnabled = false;
                        EntryDestiny.IsEnabled = false;
                        GoToDirectionsToUsePage();
                    }
                    break;
                }
            }
        }

        private async void GoToDirectionsToUsePage()
        {
            LoadingWheel.IsVisible = true;
            bool success = await App.Instance.UpdateBusPredictions();
            LoadingWheel.IsVisible = false;

            if (success)
            {
                List<DirectionToUse> directionsToUse = new List<DirectionToUse>();
                List<Direction> directions = new List<Direction>();
                foreach (var prediction in App.Instance.Predictions)
                {
                    if (!directions.Contains(prediction.Stop.Direction))
                        directions.Add(prediction.Stop.Direction);
                }
                foreach (var direction in directions)
                {
                    Stop originNearestStop = null;
                    double originNearestDistance = double.MaxValue;
                    Stop destinyNearestStop = null;
                    double destinyNearestDistance = double.MaxValue;
                    foreach (var prediction in App.Instance.Predictions)
                    {
                        if (prediction.Stop.Direction.Equals(direction))
                        {
                            double originDistance = Formulas.GetKilometersBetween(prediction.Stop.Location, originLocation.Value);
                            if (originDistance < originNearestDistance)
                            {
                                originNearestDistance = originDistance;
                                originNearestStop = prediction.Stop;
                            }
                            double destinyDistance = Formulas.GetKilometersBetween(prediction.Stop.Location, destinyLocation.Value);
                            if (destinyDistance < destinyNearestDistance)
                            {
                                destinyNearestDistance = destinyDistance;
                                destinyNearestStop = prediction.Stop;
                            }
                        }
                    }
                    if (originNearestStop.Equals(destinyNearestStop))
                        continue;

                    //System.Diagnostics.Debug.WriteLine(direction.FullName + ": " + (originNearestDistance + destinyNearestDistance).ToString());

                    BusPrediction originBus = null;
                    TimeSpan nearestTimeToGo = TimeSpan.MaxValue;
                    foreach (var prediction in App.Instance.Predictions)
                    {
                        if (prediction is BusPrediction busPrediction)
                        {
                            if (prediction.Stop.Equals(originNearestStop))
                            {
                                TimeSpan timeToGo = busPrediction.TimeOfArrival - DateTime.Now;
                                if (timeToGo < nearestTimeToGo)
                                {
                                    nearestTimeToGo = timeToGo;
                                    originBus = busPrediction;
                                }
                            }
                        }
                    }

                    if (originBus != null)
                    {
                        BusPrediction destinyBus = null;
                        foreach (var prediction in App.Instance.Predictions)
                        {
                            if (prediction is BusPrediction busPrediction)
                            {
                                if (busPrediction.BusId.Equals(originBus.BusId) && busPrediction.Stop.Equals(destinyNearestStop))
                                {
                                    destinyBus = busPrediction;
                                    break;
                                }
                            }
                        }

                        if (destinyBus != null)
                        {
                            if (destinyBus.TimeOfArrival > originBus.TimeOfArrival)
                            {
                                //System.Diagnostics.Debug.WriteLine(originBus.BusName + "; " + originBus.TimeOfArrival.ToString() + "; " + originBus.Stop.Name);
                                //System.Diagnostics.Debug.WriteLine(destinyBus.BusName + "; " + destinyBus.TimeOfArrival.ToString() + "; " + destinyBus.Stop.Name);
                                DirectionToUse directionToUse = new DirectionToUse(direction, originNearestDistance, destinyNearestDistance, originBus, destinyBus);
                                directionsToUse.Add(directionToUse);
                            }
                        }
                    }
                }

                await Navigation.PushAsync(new DirectionsToUsePage(directionsToUse));
            }
        }

        private async void ButtonOrigin_Clicked(object sender, EventArgs e)
        {
            Exception error = null;
            try
            {
                OriginWheel.IsVisible = true;
                ButtonOrigin.IsVisible = false;
                placesList.Clear();
                map.StopPins.Clear();
                map.Pins.Clear();
                var placesService = new PlacesService();
                var places = await placesService.GetPlaces(EntryOrigin.Text.ToLowerInvariant());
                foreach (var place in places)
                {
                    CustomPin placePin = new CustomPin
                    {
                        Pin = new Pin
                        {
                            Label = place.Name + " (Toque para escolher como Origem)",
                            Position = new Position(place.Location.Latitude, place.Location.Longitude),
                            Address = place.Vicinity
                        },
                        Representation = PinRepresentation.Place,
                        Id = place.Id
                    };
                    map.StopPins.Add(placePin);
                    map.Pins.Add(placePin.Pin);
                    placesList.Add(place);
                }
                map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(-29.950471, -50.983270), Distance.FromMeters(5000)));
            }
            catch (Exception ex)
            {
                error = ex;
            }
            finally
            {
                OriginWheel.IsVisible = false;
                ButtonOrigin.IsVisible = true;
            }
            if (error != null)
            {
                await DisplayAlert("Erro", "Não foi possível retornar os pontos de interesse. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
            }
        }

        private async void ButtonDestiny_Clicked(object sender, EventArgs e)
        {
            Exception error = null;
            try
            {
                DestinyWheel.IsVisible = true;
                ButtonDestiny.IsVisible = false;
                placesList.Clear();
                map.StopPins.Clear();
                map.Pins.Clear();
                var placesService = new PlacesService();
                var places = await placesService.GetPlaces(EntryDestiny.Text.ToLowerInvariant());
                foreach (var place in places)
                {
                    CustomPin placePin = new CustomPin
                    {
                        Pin = new Pin
                        {
                            Label = place.Name + " (Toque para escolher como Destino)",
                            Position = new Position(place.Location.Latitude, place.Location.Longitude),
                            Address = place.Vicinity
                        },
                        Representation = PinRepresentation.Place,
                        Id = place.Id
                    };
                    map.StopPins.Add(placePin);
                    map.Pins.Add(placePin.Pin);
                    placesList.Add(place);
                }
                map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(-29.950471, -50.983270), Distance.FromMeters(5000)));
            }
            catch (Exception ex)
            {
                error = ex;
            }
            finally
            {
                DestinyWheel.IsVisible = false;
                ButtonDestiny.IsVisible = true;
            }
            if (error != null)
            {
                await DisplayAlert("Erro", "Não foi possível retornar os pontos de interesse. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
            }
        }

        private void EntryOrigin_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (originLocation != null)
            {
                ButtonOrigin.IsEnabled = false;
            }
            else
            {
                string newOrigin = e.NewTextValue;
                ButtonOrigin.IsEnabled = !string.IsNullOrWhiteSpace(newOrigin);
            }
        }

        private void EntryDestiny_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (destinyLocation != null)
            {
                ButtonDestiny.IsEnabled = false;
            }
            else
            {
                string newDestiny = e.NewTextValue;
                ButtonDestiny.IsEnabled = !string.IsNullOrWhiteSpace(newDestiny);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            EntryOrigin.IsEnabled = true;
            EntryOrigin.Text = "";
            EntryDestiny.IsEnabled = false;
            EntryDestiny.Text = "";
            placesList = new List<Place>();
            originLocation = null;
            destinyLocation = null;
        }
    }
}