﻿using Gravatai.Model;
using Gravatai.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllRoutesPage : ContentPage
    {
        public ObservableCollection<Route> Routes { get; set; }

        public AllRoutesPage()
        {
            InitializeComponent();

            Routes = new ObservableCollection<Route>();
            ListView.ItemSelected += ListView_ItemSelected;
            ListView.BindingContext = this;
            LoadingWheel.GestureRecognizers.Add(new TapGestureRecognizer());
            LabelNoFilters.IsVisible = true;
            PickerWeekDay.SelectedIndex = (int)DateTime.Today.DayOfWeek;
            PickerWeekDay.SelectedIndexChanged += PickerWeekDay_SelectedIndexChanged;
        }

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is Route selectedRoute)
            {
                ListView.SelectedItem = null;
                await OpenTimetable(selectedRoute);
            }
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("searchbar changed: " + PickerWeekDay.SelectedIndex + " " + SearchBar.Text);
            var filter = e.NewTextValue.ToLowerInvariant();
            FilterRoutes(filter, false);
        }

        private void PickerWeekDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("picker changed: " + PickerWeekDay.SelectedIndex + " " + SearchBar.Text);
            FilterRoutes(SearchBar.Text, false);
        }

        private void FilterRoutes(string filter, bool forceRedraw)
        {
            if (!string.IsNullOrEmpty(filter))
                filter = filter.ToLowerInvariant();
            var filteredRoutes = new List<Route>();
            foreach (var route in App.Instance.AllRoutes)
                if (!filteredRoutes.Contains(route))
                {
                    var lowerRouteName = route.Name.ToLowerInvariant();
                    if (!string.IsNullOrEmpty(filter) && lowerRouteName.Contains(filter) && route.Weekdays.Contains(PickerWeekDay.SelectedIndex))
                        filteredRoutes.Add(route);
                }

            bool redraw = false;
            foreach (var route in Routes)
                if (!filteredRoutes.Contains(route))
                    redraw = true;
            foreach (var route in filteredRoutes)
                if (!Routes.Contains(route))
                    redraw = true;
            if (redraw || forceRedraw)
            {
                Routes.Clear();
                foreach (var route in filteredRoutes)
                    Routes.Add(route);
            }

            if (filter != null)
                LabelNoFilters.IsVisible = filter.Length == 0;
        }

        private async Task OpenTimetable(Route route)
        {
            Exception error = null;
            List<RouteSchedule> routeSchedules1 = null;
            List<RouteSchedule> routeSchedules2 = null;
            string direction1Name = "";
            string direction2Name = "";
            try
            {
                LoadingWheel.IsVisible = true;
                int routeId = route.Id;
                int weekday = PickerWeekDay.SelectedIndex;
                Tuple<int, int, int> direction1Tuple = new Tuple<int, int, int>(routeId, 1, weekday);
                if (App.Instance.RouteSchedulesDict.ContainsKey(direction1Tuple))
                {
                    routeSchedules1 = App.Instance.RouteSchedulesDict[direction1Tuple];
                }
                else
                {
                    var routeSchedulesService = new RouteSchedulesService();
                    var items = await routeSchedulesService.GetRouteSchedules(routeId, 1, weekday);
                    routeSchedules1 = new List<RouteSchedule>();
                    foreach (var routeSchedule in items)
                        routeSchedules1.Add(routeSchedule);
                    App.Instance.RouteSchedulesDict.Add(direction1Tuple, routeSchedules1);
                }
                Tuple<int, int, int> direction2Tuple = new Tuple<int, int, int>(routeId, 2, weekday);
                if (App.Instance.RouteSchedulesDict.ContainsKey(direction2Tuple))
                {
                    routeSchedules2 = App.Instance.RouteSchedulesDict[direction2Tuple];
                }
                else
                {
                    var routeSchedulesService = new RouteSchedulesService();
                    var items = await routeSchedulesService.GetRouteSchedules(routeId, 2, weekday);
                    routeSchedules2 = new List<RouteSchedule>();
                    foreach (var routeSchedule in items)
                        routeSchedules2.Add(routeSchedule);
                    App.Instance.RouteSchedulesDict.Add(direction2Tuple, routeSchedules2);
                }

                List<RouteStop> routeStops;
                if (App.Instance.RouteStopsDict.ContainsKey(routeId))
                {
                    routeStops = App.Instance.RouteStopsDict[routeId];
                }
                else
                {
                    var routeStopsService = new RouteStopsService();
                    var items = await routeStopsService.GetRouteStops(routeId);
                    routeStops = new List<RouteStop>();
                    foreach (var routeStop in items)
                        routeStops.Add(routeStop);
                    App.Instance.RouteStopsDict.Add(routeId, routeStops);
                }
                foreach (var routeStop in routeStops)
                {
                    if (routeStop.DirectionType == 1 && routeStop.Order == 1)
                        direction1Name = routeStop.Name;
                    if (routeStop.DirectionType == 2 && routeStop.Order == 1)
                        direction2Name = routeStop.Name;
                }
                LoadingWheel.IsVisible = false;
            }
            catch (Exception ex)
            {
                error = ex;
            }
            if (error != null)
            {
                await DisplayAlert("Erro", "Não foi possível retornar os horários do ponto escolhido. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                LoadingWheel.IsVisible = false;
            }
            else
            {
                await Navigation.PushAsync(new TimetablePage(routeSchedules1, routeSchedules2, direction1Name, direction2Name));
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            FilterRoutes(SearchBar.Text, false);
        }

        private async void OnOptions(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            Route route = (Route)menuItem.CommandParameter;
            string routeDefinition = route.StringDefinition;
            if (App.Instance.FavoriteRoutes.Contains(routeDefinition))
            {
                var result = await DisplayActionSheet("Opções", "Cancelar", null, new string[] { "Desfavoritar" });
                if ("Desfavoritar".Equals(result))
                {
                    App.Instance.RemoveFavoriteRoute(routeDefinition);
                }
            }
            else
            {
                var result = await DisplayActionSheet("Opções", "Cancelar", null, new string[] { "Favoritar" });
                if ("Favoritar".Equals(result))
                {
                    App.Instance.SaveRouteAsFavorite(routeDefinition);
                }
            }
        }
    }
}