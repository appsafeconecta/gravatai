﻿using Gravatai.Model.DeviceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        private ILocationPermissionRequester locationPermissionRequester;
        private ILocationProvider locationProvider;

        public HomePage()
        {
            InitializeComponent();

#pragma warning disable CS0618 // Type or member is obsolete

            MapImage.GestureRecognizers.Add(new TapGestureRecognizer { TappedCallback = (v, o) => { Navigation.PushAsync(new SearchMapPage()); } });
            PredictionImage.GestureRecognizers.Add(new TapGestureRecognizer { TappedCallback = (v, o) => { PredictionTypeSelection.IsVisible = true; } });
            TimetableImage.GestureRecognizers.Add(new TapGestureRecognizer { TappedCallback = (v, o) => { Navigation.PushAsync(new AllRoutesPage()); } });
            InfoImage.GestureRecognizers.Add(new TapGestureRecognizer { TappedCallback = (v, o) => { Navigation.PushAsync(new InfoPage()); } });
            ContactImage.GestureRecognizers.Add(new TapGestureRecognizer { TappedCallback = (v, o) => { Navigation.PushAsync(new ContactPage()); } });

            locationPermissionRequester = DependencyService.Get<ILocationPermissionRequester>(DependencyFetchTarget.NewInstance);
            locationPermissionRequester.RequestLocationPermissionResult += OnRequestLocationPermissionResult;
            locationProvider = DependencyService.Get<ILocationProvider>();

            PredictionTypeSelection.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = (v, o) =>
                {
                    PredictionTypeSelection.IsVisible = false;
                }
            });
            RoutesSelectionImage.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    PredictionTypeSelection.IsVisible = false;
                    await Navigation.PushAsync(new RoutesPage());
                }
            });
            NearbyStopsSelectionImage.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    PredictionTypeSelection.IsVisible = false;
                    var locationPermissionVerifier = DependencyService.Get<ILocationPermissionVerifier>();
                    if (locationPermissionVerifier.IsPermissionGranted())
                    {
                        bool isLocationEnabled = locationProvider.IsLocationEnabled();
                        if (isLocationEnabled)
                        {
                            await Navigation.PushAsync(new NearbyStopsPage());
                        }
                        else
                        {
                            var result = await DisplayAlert("Atenção", "O seu serviço de localização não está ativado.", "Abrir configurações", "Cancelar");
                            if (result)
                                locationProvider.OpenLocationSettings();
                        }
                    }
                    else
                    {
                        locationPermissionRequester.RequestLocationPermission();
                    }
                }
            });

#pragma warning restore CS0618 // Type or member is obsolete
        }

        async void OnRequestLocationPermissionResult(object sender, bool args)
        {
            if (args)
            {
                await Navigation.PushAsync(new NearbyStopsPage());
            }
            else
            {
                await DisplayAlert("Aviso", "É necessário liberar a permissão de Localização para consultar os pontos próximos a você.", "Fechar");
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //locationPermissionRequester.RequestLocationPermissionResult += OnRequestLocationPermissionResult;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            //locationPermissionRequester.RequestLocationPermissionResult -= OnRequestLocationPermissionResult;
        }

        protected override bool OnBackButtonPressed()
        {
            if (PredictionTypeSelection.IsVisible)
            {
                PredictionTypeSelection.IsVisible = false;
                return true;
            }
            else
            {
                return base.OnBackButtonPressed();
            }
        }
    }
}