﻿using Gravatai.Model;
using Gravatai.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        private BusPrediction busPrediction;
        private bool stopTimer;
        private List<Stop> stops;
        private GeographicLocation lastBusPosition;
        private double lastBusSpeed;
        private int initialNearestId;
        private int nearestId;
        private List<RoutePoint> routePoints;
        private string busAddress;

        private double simStep = 1 / 2.0;
        private double currStep = -1;
        private List<GeographicLocation> nextRouteCoordinates;

        public MapPage(BusPrediction busPrediction, List<Stop> stops, List<RoutePoint> routePoints, double initialBusSpeed, Stop currentStop)
        {
            InitializeComponent();

            Title = "Carro " + busPrediction.BusName;

            this.busPrediction = busPrediction;
            this.stops = stops;
            this.routePoints = routePoints;
            map.IsShowingUser = false;

            lastBusPosition = busPrediction.Location;
            busAddress = busPrediction.Address;
            CustomPin busPin = new CustomPin
            {
                Pin = new Pin
                {
                    Label = busPrediction.BusName,
                    Position = new Position(lastBusPosition.Latitude, lastBusPosition.Longitude),
                    Address = busAddress
                },
                Representation = PinRepresentation.Bus,
                Id = busPrediction.BusName
            };
            map.BusPins = new List<CustomPin> { busPin };
            map.Pins.Add(busPin.Pin);

            map.StopPins = new List<CustomPin>();
            //map.StopPins.Add(busPin);
            //map.Pins.Add(busPin.Pin);
            foreach (var stop in stops)
            {
                // Only show current stop
                if (stop.Id != currentStop.Id) continue;
                CustomPin stopPin = new CustomPin
                {
                    Pin = new Pin
                    {
                        Label = stop.Name,
                        Position = new Position(stop.Location.Latitude, stop.Location.Longitude),
                        Address = stop.Address
                    },
                    Representation = PinRepresentation.Stop,
                    Id = stop.Name
                };
                map.StopPins.Add(stopPin);
                map.Pins.Add(stopPin.Pin);
            }

            initialNearestId = GetNearestIdFrom(busPrediction.Location);
            nearestId = initialNearestId;
            lastBusSpeed = nearestId <= 1 || busPrediction.TimeSinceLastUpdate > TimeSpan.FromMinutes(2) ? 0.0 : initialBusSpeed;
            nextRouteCoordinates = new List<GeographicLocation>();
            foreach (var routePoint in routePoints)
            {
                map.RouteCoordinates.Add(new Position(routePoint.Location.Latitude, routePoint.Location.Longitude));
                if (routePoint.Id > nearestId)
                    nextRouteCoordinates.Add(routePoint.Location);
            }
            if (nextRouteCoordinates.Count == 1)
                lastBusSpeed = 0.0;

            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(busPrediction.Location.Latitude, busPrediction.Location.Longitude), Distance.FromMeters(750)));

            UpdateBus();
        }

        private void UpdateBus()
        {
            Device.StartTimer(TimeSpan.FromSeconds(simStep), () =>
            {
                if (stopTimer)
                    return false;

                Device.BeginInvokeOnMainThread(() =>
                {
                    int currRoutePointId = (int)Math.Floor(currStep);
                    GeographicLocation currPos;
                    GeographicLocation nextPos;
                    if (nextRouteCoordinates.Count > currRoutePointId && currRoutePointId >= 0 && currRoutePointId > (nearestId - initialNearestId - 1))
                        currPos = nextRouteCoordinates[currRoutePointId];
                    else
                        currPos = lastBusPosition;
                    if (nextRouteCoordinates.Count > currRoutePointId + 1 && currRoutePointId + 1 >= 0 && currRoutePointId + 1 > (nearestId - initialNearestId - 1))
                        nextPos = nextRouteCoordinates[currRoutePointId + 1];
                    else
                        nextPos = lastBusPosition;
                    double latDiff = nextPos.Latitude - currPos.Latitude;
                    double lngDiff = nextPos.Longitude - currPos.Longitude;
                    double interpolation = currStep - currRoutePointId;
                    Position simPos = new Position(currPos.Latitude + latDiff * interpolation, currPos.Longitude + lngDiff * interpolation);
                    CustomPin busPin = new CustomPin
                    {
                        Pin = new Pin
                        {
                            Label = map.BusPins[0].Pin.Label,
                            Position = simPos,
                            Address = busAddress
                        },
                        Representation = PinRepresentation.Bus,
                        Id = map.BusPins[0].Pin.Label
                    };
                    map.BusPins = new List<CustomPin> { busPin };

                    double distance = Formulas.GetKilometersBetween(nextPos, currPos);
                    double speed = lastBusSpeed / 3600.0;
                    if (distance > 0)
                        currStep += (speed * simStep) / distance;
                });

                return true;
            });

            Device.StartTimer(TimeSpan.FromSeconds(30), () =>
            {
                System.Diagnostics.Debug.WriteLine("Update Real Bus");

                if (stopTimer)
                    return false;

                Task.Run(async () =>
                {
                    await App.Instance.UpdateBusPredictions(true);

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        BusPrediction trackingBus = GetTrackingBus();
                        if (trackingBus != null)
                        {
                            lastBusPosition = trackingBus.Location;
                            busAddress = trackingBus.Address;
                            nearestId = GetNearestIdFrom(trackingBus.Location);
                            if (nearestId <= 1 || trackingBus.TimeSinceLastUpdate > TimeSpan.FromMinutes(2))
                            {
                                lastBusSpeed = 0.0;
                            }
                            else
                            {
                                var busSpeedService = new BusSpeedService();
                                lastBusSpeed = await busSpeedService.GetBusSpeed(trackingBus);
                            }
                            currStep = nearestId - initialNearestId - 1;
                            System.Diagnostics.Debug.WriteLine(lastBusPosition.Latitude + ", " + lastBusPosition.Longitude + " (" + lastBusSpeed + ")");
                        }
                    });
                });

                return true;
            });
        }

        private int GetNearestIdFrom(GeographicLocation location)
        {
            int nearestId = 0;
            double shortestDistance = Double.MaxValue;
            foreach (var routePoint in routePoints)
            {
                double distance = Formulas.GetKilometersBetween(routePoint.Location, location);
                if (distance < shortestDistance)
                {
                    shortestDistance = distance;
                    nearestId = routePoint.Id;
                }
            }
            return nearestId;
        }

        private BusPrediction GetTrackingBus()
        {
            foreach (var prediction in App.Instance.Predictions)
                if (prediction is BusPrediction busPrediction)
                    if (busPrediction.BusId == this.busPrediction.BusId)
                        return busPrediction;
            return null;
        }

        protected override void OnDisappearing()
        {
            stopTimer = true;
            base.OnDisappearing();
        }
    }
}