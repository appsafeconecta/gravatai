﻿using Gravatai.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FavoriteRoutesPage : ContentPage
    {
        public FavoriteRoutesPage()
        {
            InitializeComponent();

            ListView.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is FavoriteRoute favoriteRoute)
            {
                ListView.SelectedItem = null;
                if (favoriteRoute.Route != null)
                    Navigation.PushAsync(new DirectionsPage(favoriteRoute.Route));
            }
        }

        private void UpdateListView()
        {
            var favoriteRoutes = new List<FavoriteRoute>();
            foreach (var favRouteStr in App.Instance.FavoriteRoutes)
            {
                var favoriteRoute = new FavoriteRoute(favRouteStr);
                foreach (var prediction in App.Instance.Predictions)
                {
                    string def = prediction.Stop.Direction.Route.StringDefinition;
                    if (def.Equals(favoriteRoute.StringDefinition))
                    {
                        favoriteRoute.Route = prediction.Stop.Direction.Route;
                        break;
                    }
                }
                favoriteRoutes.Add(favoriteRoute);
            }
            ListView.ItemsSource = favoriteRoutes;
            LabelNoFavoriteRoutes.IsVisible = (favoriteRoutes.Count == 0);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            UpdateListView();
        }

        private void OnUnfavorite(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            FavoriteRoute favoriteRoute = (FavoriteRoute)menuItem.CommandParameter;
            string routeDefinition = favoriteRoute.StringDefinition;
            if (App.Instance.FavoriteRoutes.Contains(routeDefinition))
            {
                App.Instance.RemoveFavoriteRoute(routeDefinition);
                UpdateListView();
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Application.Current.MainPage = new MainPage();
            return true;
        }
    }
}