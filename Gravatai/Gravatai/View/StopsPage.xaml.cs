﻿using Gravatai.Model;
using Gravatai.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StopsPage : ContentPage
    {
        private Direction direction;

        public ObservableCollection<Stop> Stops { get; set; }

        public StopsPage(Direction direction)
        {
            InitializeComponent();

            Title = direction.Name + " (" + direction.Route.Name + ")";

            this.direction = direction;
            Stops = new ObservableCollection<Stop>();
            ListView.ItemSelected += ListView_ItemSelected;
            ListView.BindingContext = this;
            LoadingWheel.GestureRecognizers.Add(new TapGestureRecognizer());
        }

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is Stop selectedStop)
            {
                ListView.SelectedItem = null;
                await OpenPredictions(selectedStop);
            }
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            var filter = e.NewTextValue.ToLowerInvariant();
            FilterStops(filter, false);
        }

        private void FilterStops(string filter, bool forceRedraw)
        {
            if (!string.IsNullOrEmpty(filter))
                filter = filter.ToLowerInvariant();
            var filteredStops = new List<Stop>();
            foreach (var prediction in App.Instance.Predictions)
                if (prediction.Stop.Direction.Equals(direction) && !filteredStops.Contains(prediction.Stop))
                {
                    var lowerStopName = prediction.Stop.Name.ToLowerInvariant();
                    if (string.IsNullOrEmpty(filter) || lowerStopName.Contains(filter))
                        filteredStops.Add(prediction.Stop);
                }

            bool redraw = false;
            foreach (var stop in Stops)
                if (!filteredStops.Contains(stop))
                    redraw = true;
            foreach (var stop in filteredStops)
                if (!Stops.Contains(stop))
                    redraw = true;
            if (redraw || forceRedraw)
            {
                Stops.Clear();
                foreach (var stop in filteredStops)
                    Stops.Add(stop);
            }
        }

        private async Task OpenPredictions(Stop stop)
        {
            Exception error = null;
            List<RouteSchedule> routeSchedules = null;
            double stopInterpolation = 0;
            try
            {
                LoadingWheel.IsVisible = true;
                int routeId = stop.Direction.Route.Id;
                int directionType = stop.Direction.Type;
                int weekday = (int)DateTime.Today.DayOfWeek;
                Tuple<int, int, int> directionTuple = new Tuple<int, int, int>(routeId, directionType, weekday);
                if (App.Instance.RouteSchedulesDict.ContainsKey(directionTuple))
                {
                    routeSchedules = App.Instance.RouteSchedulesDict[directionTuple];
                }
                else
                {
                    var routeSchedulesService = new RouteSchedulesService();
                    var items = await routeSchedulesService.GetRouteSchedules(routeId, directionType, weekday);
                    routeSchedules = new List<RouteSchedule>();
                    foreach (var routeSchedule in items)
                        routeSchedules.Add(routeSchedule);
                    App.Instance.RouteSchedulesDict.Add(directionTuple, routeSchedules);
                }
                List<RouteStop> routeStops;
                if (App.Instance.RouteStopsDict.ContainsKey(routeId))
                {
                    routeStops = App.Instance.RouteStopsDict[routeId];
                }
                else
                {
                    var routeStopsService = new RouteStopsService();
                    var items = await routeStopsService.GetRouteStops(routeId);
                    routeStops = new List<RouteStop>();
                    foreach (var routeStop in items)
                        routeStops.Add(routeStop);
                    App.Instance.RouteStopsDict.Add(routeId, routeStops);
                }
                int maxOrder = 0;
                int thisOrder = -1;
                foreach (var routeStop in routeStops)
                    if (routeStop.DirectionType == directionType)
                    {
                        maxOrder++;
                        if (routeStop.Id == stop.Id)
                            thisOrder = routeStop.Order;
                    }
                stopInterpolation = (double)(thisOrder - 1) / Math.Max((maxOrder - 1), 1);
                LoadingWheel.IsVisible = false;
            }
            catch (Exception ex)
            {
                error = ex;
            }
            if (error != null)
            {
                await DisplayAlert("Erro", "Não foi possível retornar as previsões do ponto escolhido. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                LoadingWheel.IsVisible = false;
            }
            else
            {
                await Navigation.PushAsync(new PredictionsPage(stop, routeSchedules, stopInterpolation));
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            FilterStops(SearchBar.Text, false);

            //await App.Instance.UpdateBusPredictions();
            //FilterStops(SearchBar.Text, true);
        }

        private async void OnOptions(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            Stop stop = (Stop)menuItem.CommandParameter;
            string stopDefinition = stop.StringDefinition;
            if (App.Instance.FavoriteStops.Contains(stopDefinition))
            {
                var result = await DisplayActionSheet("Opções", "Cancelar", null, new string[] { "Desfavoritar" });
                if ("Desfavoritar".Equals(result))
                {
                    App.Instance.RemoveFavoriteStop(stopDefinition);
                }
            }
            else
            {
                var result = await DisplayActionSheet("Opções", "Cancelar", null, new string[] { "Favoritar" });
                if ("Favoritar".Equals(result))
                {
                    App.Instance.SaveStopAsFavorite(stopDefinition);
                }
            }
        }
    }
}