﻿using Gravatai.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gravatai.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TimetablePage : ContentPage
    {
        public ObservableCollection<RouteScheduleGroup> Direction1Schedules { get; set; }
        public ObservableCollection<RouteScheduleGroup> Direction2Schedules { get; set; }

        public TimetablePage(List<RouteSchedule> routeSchedules1, List<RouteSchedule> routeSchedules2, string direction1Name, string direction2Name)
        {
            InitializeComponent();

            LabelDirection1Title.Text = direction1Name;
            LabelDirection2Title.Text = direction2Name;
            Direction1Schedules = new ObservableCollection<RouteScheduleGroup>();
            for (int i = 0; i < routeSchedules1.Count; i += 4)
            {
                string s1 = routeSchedules1.Count > i + 0 ? routeSchedules1[i + 0].Start : "";
                string s2 = routeSchedules1.Count > i + 1 ? routeSchedules1[i + 1].Start : "";
                string s3 = routeSchedules1.Count > i + 2 ? routeSchedules1[i + 2].Start : "";
                string s4 = routeSchedules1.Count > i + 3 ? routeSchedules1[i + 3].Start : "";
                RouteScheduleGroup group = new RouteScheduleGroup(s1, s2, s3, s4);
                Direction1Schedules.Add(group);
            }
            ListView1.ItemSelected += ListView1_ItemSelected;
            ListView1.BindingContext = this;
            Direction2Schedules = new ObservableCollection<RouteScheduleGroup>();
            for (int i = 0; i < routeSchedules2.Count; i += 4)
            {
                string s1 = routeSchedules2.Count > i + 0 ? routeSchedules2[i + 0].Start : "";
                string s2 = routeSchedules2.Count > i + 1 ? routeSchedules2[i + 1].Start : "";
                string s3 = routeSchedules2.Count > i + 2 ? routeSchedules2[i + 2].Start : "";
                string s4 = routeSchedules2.Count > i + 3 ? routeSchedules2[i + 3].Start : "";
                RouteScheduleGroup group = new RouteScheduleGroup(s1, s2, s3, s4);
                Direction2Schedules.Add(group);
            }
            ListView2.ItemSelected += ListView2_ItemSelected;
            ListView2.BindingContext = this;
        }

        private void ListView1_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is RouteScheduleGroup selectedGroup)
            {
                ListView1.SelectedItem = null;
            }
        }

        private void ListView2_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is RouteScheduleGroup selectedGroup)
            {
                ListView2.SelectedItem = null;
            }
        }
    }
}