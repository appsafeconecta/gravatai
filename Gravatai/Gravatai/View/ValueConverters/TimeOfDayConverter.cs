﻿using Gravatai.Model;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace Gravatai.View.ValueConverters
{
    public class TimeOfDayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime timeOfArrival = (DateTime)value;
            if (timeOfArrival < DateTime.Now)
                timeOfArrival = DateTime.Now;
            bool moreThanOneHourToGo = false;
            if (timeOfArrival > DateTime.Now + TimeSpan.FromMinutes(60))
            {
                timeOfArrival = DateTime.Now + TimeSpan.FromMinutes(60);
                moreThanOneHourToGo = true;
            }
            TimeSpan timeOfDay = timeOfArrival.TimeOfDay;
            return (moreThanOneHourToGo ? "+" : "") + timeOfDay.ToString(@"hh\:mm");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
