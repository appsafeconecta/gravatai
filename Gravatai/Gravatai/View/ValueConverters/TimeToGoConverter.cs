﻿using Gravatai.Model;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace Gravatai.View.ValueConverters
{
    public class TimeToGoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime timeOfArrival = (DateTime)value;
            TimeSpan timeToGo = timeOfArrival - DateTime.Now;
            if (timeToGo.TotalMinutes > 60)
                return "+60 min";
            else if (timeToGo.TotalMinutes < 1)
                return "< 1 min";
            else
                return (int)timeToGo.TotalMinutes + " min";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
