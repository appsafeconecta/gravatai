﻿using Gravatai.Model;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace Gravatai.View.ValueConverters
{
    public class FavoriteRouteLabelColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool hasPrediction = (bool)value;
            if (hasPrediction)
                return "#333333";
            else
                return "#a9a9a9";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
