﻿using Gravatai.Model;
using Gravatai.Service;
using Gravatai.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Gravatai
{
    public partial class App : Application
    {
        public static App Instance { get; private set; }

        public List<Route> AllRoutes { get; private set; }
        public List<Prediction> Predictions { get; private set; }
        private DateTime lastPredictionsUpdateTime;

        public GeographicLocation? UserLocation { get; set; }
        public Dictionary<Tuple<int, int>, List<RoutePoint>> RoutePointsDict { get; set; }
        public Dictionary<Tuple<int, int, int>, List<RouteSchedule>> RouteSchedulesDict { get; set; }
        public Dictionary<int, List<RouteStop>> RouteStopsDict { get; set; }

        public List<string> FavoriteRoutes { get; set; }
        private string FavoriteRoutesString { get { return "[ " + string.Join(", ", FavoriteRoutes.Select(o => "\"" + o + "\"")) + " ]"; } }
        public List<string> FavoriteStops { get; set; }
        private string FavoriteStopsString { get { return "[ " + string.Join(", ", FavoriteStops.Select(o => "\"" + o + "\"")) + " ]"; } }

        public App()
        {
            InitializeComponent();
            Instance = this;

            Predictions = new List<Prediction>();
            UserLocation = null;
            RoutePointsDict = new Dictionary<Tuple<int, int>, List<RoutePoint>>();
            RouteSchedulesDict = new Dictionary<Tuple<int, int, int>, List<RouteSchedule>>();
            RouteStopsDict = new Dictionary<int, List<RouteStop>>();
        }

        public async Task<bool> UpdateBusPredictions(bool forceUpdate = false)
        {
            System.Diagnostics.Debug.WriteLine("Request update");
            if (DateTime.Now > lastPredictionsUpdateTime + TimeSpan.FromSeconds(30) || forceUpdate)
            {
                System.Diagnostics.Debug.WriteLine("Perform update");
                Exception error = null;
                try
                {

                    /*
                    var timetablePredictionsService = new TimetableServiceSoap();
                    var timetablePredictions = await timetablePredictionsService.GetTimetablePredictions();
                    */

                    if (AllRoutes == null)
                    {
                        var allRoutesService = new AllRoutesService();
                        var allRoutes = await allRoutesService.GetAllRoutes();
                        AllRoutes = new List<Route>(allRoutes);
                    }

                    var busPredictionsService = new BusPredictionsService();
                    var items = await busPredictionsService.GetBusPredictions("http://irogcn.com.br/Mobile/00040490530540510480520570480530510480530520480530491563/PontoParadaMobile/RetornarInformacoesJson");

                    Predictions.Clear();
                    foreach (var busPrediction in items)
                    {
                        Predictions.Add(busPrediction);
                    }
                }
                catch (Exception ex)
                {
                    error = ex;
                }
                if (error != null)
                {
                    await MainPage.DisplayAlert("Erro", "Não foi possível retornar as previsões. Por favor, verifique sua conexão com a Internet e tente novamente.", "OK");
                    return false;
                }
                else
                {
                    lastPredictionsUpdateTime = DateTime.Now;
                }
            }
            return true;
        }

        public async void SaveRouteAsFavorite(string routeDefinition)
        {
            if (FavoriteRoutes.Contains(routeDefinition))
                return;

            foreach (var route in AllRoutes)
            {
                string def = route.StringDefinition;
                if (def.Equals(routeDefinition))
                {
                    route.IsFavorite = true;
                    break;
                }
            }
            FavoriteRoutes.Add(routeDefinition);
            Properties["FavoriteRoutes"] = FavoriteRoutesString;
            await SavePropertiesAsync();
        }

        public async void RemoveFavoriteRoute(string routeDefinition)
        {
            if (!FavoriteRoutes.Contains(routeDefinition))
                return;

            foreach (var route in AllRoutes)
            {
                string def = route.StringDefinition;
                if (def.Equals(routeDefinition))
                {
                    route.IsFavorite = false;
                    break;
                }
            }
            FavoriteRoutes.Remove(routeDefinition);
            Properties["FavoriteRoutes"] = FavoriteRoutesString;
            await SavePropertiesAsync();
        }

        public async void SaveStopAsFavorite(string stopDefinition)
        {
            if (FavoriteStops.Contains(stopDefinition))
                return;

            foreach (var prediction in Predictions)
            {
                string def = prediction.Stop.StringDefinition;
                if (def.Equals(stopDefinition))
                {
                    prediction.Stop.IsFavorite = true;
                    break;
                }
            }
            FavoriteStops.Add(stopDefinition);
            Properties["FavoriteStops"] = FavoriteStopsString;
            await SavePropertiesAsync();
        }

        public async void RemoveFavoriteStop(string stopDefinition)
        {
            if (!FavoriteStops.Contains(stopDefinition))
                return;

            foreach (var prediction in Predictions)
            {
                string def = prediction.Stop.StringDefinition;
                if (def.Equals(stopDefinition))
                {
                    prediction.Stop.IsFavorite = false;
                    break;
                }
            }
            FavoriteStops.Remove(stopDefinition);
            Properties["FavoriteStops"] = FavoriteStopsString;
            await SavePropertiesAsync();
        }

        protected async override void OnStart()
        {
            if (Properties.ContainsKey("FavoriteRoutes"))
            {
                var json = (string)Properties["FavoriteRoutes"];
                FavoriteRoutes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(json);
            }
            else
            {
                FavoriteRoutes = new List<string>();
                Properties.Add("FavoriteRoutes", FavoriteRoutesString);
            }

            if (Properties.ContainsKey("FavoriteStops"))
            {
                var json = (string)Properties["FavoriteStops"];
                FavoriteStops = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(json);
            }
            else
            {
                FavoriteStops = new List<string>();
                Properties.Add("FavoriteStops", FavoriteStopsString);
            }

            MainPage = new SplashPage();

            bool success = await UpdateBusPredictions();
            while (!success)
                success = await UpdateBusPredictions();

            MainPage = new MainPage();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
