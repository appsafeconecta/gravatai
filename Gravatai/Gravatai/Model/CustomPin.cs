﻿using Xamarin.Forms.Maps;

namespace Gravatai.Model
{
    public enum PinRepresentation { Stop, Bus, Place }
    public class CustomPin
    {
        public Pin Pin { get; set; }
        public PinRepresentation Representation { get; set; }
        public string Id { get; set; }
    }
}
