﻿using System;

namespace Gravatai.Model
{
    public class BusPrediction : Prediction
    {
        public int Distance { get; private set; }
        public GeographicLocation Location { get; private set; }
        public string Address { get; private set; }
        public int BusId { get; private set; }

        public BusPrediction(DateTime timeOfArrival, Stop stop, string busName, bool accessible, int distance, GeographicLocation location, string address, int busId)
            : base(timeOfArrival, stop, busName, accessible)
        {
            Distance = distance;
            Location = location;
            Address = address;
            BusId = busId;
        }
    }
}
