﻿
namespace Gravatai.Model
{
    public struct GeographicLocation
    {
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }

        public GeographicLocation(double latitude, double longitude) : this()
        {
            Latitude = latitude % 90;
            Longitude = longitude % 180;
        }

        public override bool Equals(object obj)
        {
            GeographicLocation location = (GeographicLocation)obj;
            return location.Latitude == Latitude && location.Longitude == Longitude;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 31 + Latitude.GetHashCode();
            hash = hash * 31 + Longitude.GetHashCode();
            return hash;
        }

        public override string ToString()
        {
            return Latitude.ToString() + ", " + Longitude.ToString();
        }
    }
}
