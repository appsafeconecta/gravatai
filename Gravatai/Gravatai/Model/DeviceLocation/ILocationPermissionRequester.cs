﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model.DeviceLocation
{
    public interface ILocationPermissionRequester
    {
        event EventHandler<bool> RequestLocationPermissionResult;
        void RequestLocationPermission();
    }
}
