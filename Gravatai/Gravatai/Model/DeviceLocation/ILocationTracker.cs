﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model.DeviceLocation
{
    public interface ILocationTracker
    {
        event EventHandler<GeographicLocation> LocationChanged;
        void StartTracking();
        void PauseTracking();
    }
}
