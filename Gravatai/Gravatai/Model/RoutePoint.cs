﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model
{
    public class RoutePoint
    {
        public int Id { get; private set; }
        public GeographicLocation Location { get; private set; }

        public RoutePoint(int id, double latitude, double longitude)
        {
            Id = id;
            Location = new GeographicLocation(latitude, longitude);
        }
    }
}
