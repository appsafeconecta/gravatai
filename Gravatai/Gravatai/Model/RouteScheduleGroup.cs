﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model
{
    public class RouteScheduleGroup
    {
        public string Schedule1 { get; private set; }
        public string Schedule2 { get; private set; }
        public string Schedule3 { get; private set; }
        public string Schedule4 { get; private set; }

        public bool IsVisible1 { get; private set; }
        public bool IsVisible2 { get; private set; }
        public bool IsVisible3 { get; private set; }
        public bool IsVisible4 { get; private set; }

        public RouteScheduleGroup(string s1, string s2, string s3, string s4)
        {
            Schedule1 = s1;
            Schedule2 = s2;
            Schedule3 = s3;
            Schedule4 = s4;

            IsVisible1 = !string.IsNullOrWhiteSpace(s1);
            IsVisible2 = !string.IsNullOrWhiteSpace(s2);
            IsVisible3 = !string.IsNullOrWhiteSpace(s3);
            IsVisible4 = !string.IsNullOrWhiteSpace(s4);
        }
    }
}
