﻿using System;
using System.Threading.Tasks;

namespace Gravatai.Model.Mail
{
    public interface IMailSender
    {
        Task Send(string name, string email, string type, string subject, string message, string bus, string route, string telephone, bool wantsResponse, DateTime date);
    }
}
