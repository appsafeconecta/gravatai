﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model
{
    public class DirectionToUse
    {
        public Direction Direction { get; private set; }
        public double OriginDistance { get; private set; }
        public double DestinyDistance { get; private set; }
        public BusPrediction OriginBusPrediction { get; private set; }
        public BusPrediction DestinyBusPrediction { get; private set; }

        public string OriginWalkString { get { return "Andar " + OriginDistance.ToString("0.00") + " km até " + OriginBusPrediction.Stop.Name; } }
        public string OriginBusTimeString { get { return "Pegar ônibus " + OriginBusPrediction.BusName + " em " + OriginBusPrediction.Stop.Name + " às " + OriginBusPrediction.TimeOfArrival.TimeOfDay.ToString(@"hh\:mm"); } }
        public string DestinyBusTimeString { get { return "Ônibus " + DestinyBusPrediction.BusName + " chega às " + DestinyBusPrediction.TimeOfArrival.TimeOfDay.ToString(@"hh\:mm") + " em " + DestinyBusPrediction.Stop.Name; } }
        public string DestinyWalkString { get { return "Andar " + DestinyDistance.ToString("0.00") + " km até o seu destino"; } }

        public DirectionToUse(Direction direction, double originDistance, double destinyDistance, BusPrediction originBusPrediction, BusPrediction destinyBusPrediction)
        {
            Direction = direction;
            OriginDistance = originDistance;
            DestinyDistance = destinyDistance;
            OriginBusPrediction = originBusPrediction;
            DestinyBusPrediction = destinyBusPrediction;
        }
    }
}
