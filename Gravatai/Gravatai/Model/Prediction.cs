﻿using System;
using System.ComponentModel;

namespace Gravatai.Model
{
    public abstract class Prediction : INotifyPropertyChanged
    {
        public Stop Stop { get; private set; }
        public bool Accessible { get; private set; }
        public string BusName { get; private set; }
        public TimeSpan TimeSinceLastUpdate { get; set; }

        private DateTime timeOfArrival;
        public DateTime TimeOfArrival { get { return timeOfArrival; } set { timeOfArrival = value; OnPropertyChanged(this, "TimeOfArrival"); } }

        public Prediction(DateTime timeOfArrival, Stop stop, string busName, bool accessible)
        {
            TimeOfArrival = timeOfArrival;
            Stop = stop;
            BusName = busName;
            Accessible = accessible;
        }

        public override bool Equals(object obj)
        {
            var prediction = obj as Prediction;
            if (prediction == null)
                return false;
            return this.Stop.Equals(prediction.Stop) && this.BusName.Equals(prediction.BusName);
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 31 + Stop.GetHashCode();
            hash = hash * 31 + BusName.GetHashCode();
            return hash;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(object sender, string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(sender, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
