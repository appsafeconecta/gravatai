﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model
{
    public class Place
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public GeographicLocation Location { get; private set; }
        public string Vicinity { get; private set; }

        public Place(string id, string name, GeographicLocation location, string vicinity)
        {
            Id = id;
            Name = name;
            Location = location;
            Vicinity = vicinity;
        }
    }
}
