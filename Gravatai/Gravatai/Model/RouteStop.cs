﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model
{
   public class RouteStop
    {
        public int Id { get; private set; }
        public int Order { get; private set; }
        public int DirectionType { get; set; }
        public string Name { get; set; }

        public RouteStop(int id, int order, int directionType, string name)
        {
            Id = id;
            Order = order;
            DirectionType = directionType;
            Name = name;
        }
    }
}
