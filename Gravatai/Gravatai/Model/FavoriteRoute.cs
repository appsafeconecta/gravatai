﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model
{
    public class FavoriteRoute
    {
        public string StringDefinition { get; private set; }
        public Route Route { get; set; }

        public string Name { get { return StringDefinition.Substring(StringDefinition.IndexOf("#$#") + 3); } }
        public bool HasPrediction { get { return Route != null; } }

        public FavoriteRoute(string stringDefinition)
        {
            StringDefinition = stringDefinition;
            Route = null;
        }
    }
}
