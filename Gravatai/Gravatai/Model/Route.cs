﻿
using System.Collections.Generic;
using System.ComponentModel;

namespace Gravatai.Model
{
    public class Route : INotifyPropertyChanged
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public List<GeographicLocation> RoutePoints { get; private set; }
        public List<int> Weekdays { get; private set; }

        private bool isFavorite;
        public bool IsFavorite { get { return isFavorite; } set { isFavorite = value; OnPropertyChanged(this, "IsFavorite"); } }

        public string StringDefinition { get { return Id + "#$#" + Name; } }

        public Route(int id, string name)
        {
            Id = id;
            Name = name;
            Weekdays = new List<int>();
        }

        public void SetRoutePoints(List<GeographicLocation> routePoints)
        {
            RoutePoints = routePoints;
        }

        public override bool Equals(object obj)
        {
            var route = obj as Route;
            if (route == null)
                return false;
            return this.Id.Equals(route.Id);
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 31 + Id.GetHashCode();
            return hash;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(object sender, string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(sender, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
