﻿using System;

namespace Gravatai.Model
{
    public class TimetablePrediction : Prediction
    {
        public TimetablePrediction(DateTime timeOfArrival, Stop stop)
            : base(timeOfArrival, stop, "-", false)
        {
            TimeSinceLastUpdate = TimeSpan.Zero;
        }
    }
}
