﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model
{
    public class FavoriteStop
    {
        public string StringDefinition { get; private set; }
        public Stop Stop { get; set; }

        public string Name { get { return StringDefinition.Substring(StringDefinition.IndexOf("#$#") + 3, StringDefinition.IndexOf("$#$") - (StringDefinition.IndexOf("#$#") + 3)); } }
        public string RouteName { get { return StringDefinition.Substring(StringDefinition.IndexOf("$#$") + 3); } }
        public bool HasPrediction { get { return Stop != null; } }

        public FavoriteStop(string stringDefinition)
        {
            StringDefinition = stringDefinition;
            Stop = null;
        }
    }
}
