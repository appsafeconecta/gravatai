﻿
namespace Gravatai.Model
{
    public class Direction
    {
        public Route Route { get; private set; }
        public string Name { get; private set; }
        public int Type { get; private set; }

        public string FullName { get { return Route.Name + " (" + Name + ")"; } }

        public Direction(Route route, string name, int type)
        {
            Route = route;
            Name = name;
            Type = type;
        }

        public override bool Equals(object obj)
        {
            var direction = obj as Direction;
            if (direction == null)
                return false;
            return this.Type.Equals(direction.Type) && this.Route.Equals(direction.Route);
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 31 + Type.GetHashCode();
            hash = hash * 31 + Route.GetHashCode();
            return hash;
        }
    }
}
