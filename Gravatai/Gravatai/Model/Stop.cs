﻿
using System.ComponentModel;

namespace Gravatai.Model
{
    public class Stop : INotifyPropertyChanged
    {
        public int Id { get; private set; }
        public Direction Direction { get; private set; }
        public string Name { get; private set; }
        public GeographicLocation Location { get; private set; }
        public string Address { get; private set; }
        public double DistanceFromUser { get; set; }

        private bool isFavorite;
        public bool IsFavorite { get { return isFavorite; } set { isFavorite = value; OnPropertyChanged(this, "IsFavorite"); } }

        public string StringDefinition { get { return Id + "e" + Direction.Type + "e" + Direction.Route.Id + "#$#" + Name + "$#$" + Direction.FullName; } }

        public Stop(int id, Direction direction, string name, GeographicLocation location, string address)
        {
            Id = id;
            Direction = direction;
            Name = name;
            Location = location;
            Address = address;
        }

        public override bool Equals(object obj)
        {
            var stop = obj as Stop;
            if (stop == null)
                return false;
            return this.Id.Equals(stop.Id) && this.Direction.Equals(stop.Direction);
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 31 + Id.GetHashCode();
            hash = hash * 31 + Direction.GetHashCode();
            return hash;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(object sender, string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(sender, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
