﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model
{
    public class RouteSchedule
    {
        public string Start { get; private set; }
        public string Finish { get; private set; }

        public RouteSchedule(string start, string finish)
        {
            Start = start;
            Finish = finish;
        }
    }
}
