﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gravatai.Model
{
    public static class Formulas
    {
        public static double GetKilometersBetween(GeographicLocation location1, GeographicLocation location2)
        {
            double degreesToRad = Math.PI / 180f;
            double R = 6371;
            double dLat = (location2.Latitude - location1.Latitude) * degreesToRad;
            double dLon = (location2.Longitude - location1.Longitude) * degreesToRad;
            double lat1rad = location1.Latitude * degreesToRad;
            double lat2rad = location2.Latitude * degreesToRad;

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1rad) * Math.Cos(lat2rad);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return R * c;
        }
    }
}
