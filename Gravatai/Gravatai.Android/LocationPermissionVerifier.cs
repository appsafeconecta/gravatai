﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Gravatai.Model.DeviceLocation;
using Xamarin.Forms;
using Android.Support.V4.Content;
using Android.Content.PM;
using Android;

[assembly: Dependency(typeof(Gravatai.Droid.LocationPermissionVerifier))]
namespace Gravatai.Droid
{
    public class LocationPermissionVerifier : ILocationPermissionVerifier
    {
        public LocationPermissionVerifier()
        {

        }

        public bool IsPermissionGranted()
        {
            const string permission = Manifest.Permission.AccessFineLocation;
            return ContextCompat.CheckSelfPermission(Toolkit.Activity, permission) == (int)Permission.Granted;
        }
    }
}