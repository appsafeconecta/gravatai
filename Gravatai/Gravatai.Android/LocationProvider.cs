﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Gravatai.Model.DeviceLocation;
using Android.Locations;

[assembly: Dependency(typeof(Gravatai.Droid.LocationProvider))]
namespace Gravatai.Droid
{
    public class LocationProvider : ILocationProvider
    {
        Activity activity;
        LocationManager locationManager;

        public LocationProvider()
        {
            activity = Toolkit.Activity;

            if (activity == null)
                throw new InvalidOperationException("Must call Toolkit.Init before using LocationProvider");

            locationManager = activity.GetSystemService(Context.LocationService) as LocationManager;
        }

        public bool IsLocationEnabled()
        {
            return locationManager.IsProviderEnabled(LocationManager.GpsProvider);
        }

        public void OpenLocationSettings()
        {
            Intent callLocationSettingIntent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
            activity.StartActivity(callLocationSettingIntent);
        }
    }
}