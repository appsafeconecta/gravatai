﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Gravatai.Droid.CustomRenderers;
using Gravatai.View.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(AndroidTimePicker))]
namespace Gravatai.Droid.CustomRenderers
{
    public class AndroidTimePicker : TimePickerRenderer
    {
        private EditText TextField { get; set; }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.TimePicker> e)
        {
            base.OnElementChanged(e);

            if (TextField == null)
            {
                TextField = new EditText(this.Context)
                {
                    Focusable = false,
                    Clickable = false,
                    Tag = this
                };

                TextField.Click += TextField_Click;
                SetNativeControl(TextField);
            }

            TextField.Text = DateTime.Today.Add(Element.Time).ToString(Element.Format);
        }

        private void TextField_Click(object sender, EventArgs e)
        {
            new TimePickerDialog(this.Context, new EventHandler<TimePickerDialog.TimeSetEventArgs>(OnTimeSet), Element.Time.Hours, Element.Time.Minutes, true).Show();
        }

        private void OnTimeSet(object sender, TimePickerDialog.TimeSetEventArgs e)
        {
            Element.Time = new TimeSpan(e.HourOfDay, e.Minute, 0);
            TextField.Text = DateTime.Today.Add(Element.Time).ToString(Element.Format);
        }
    }
}