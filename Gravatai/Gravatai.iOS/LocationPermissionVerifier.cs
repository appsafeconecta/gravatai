﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Gravatai.Model.DeviceLocation;
using Xamarin.Forms;
using CoreLocation;

[assembly: Dependency(typeof(Gravatai.iOS.LocationPermissionVerifier))]
namespace Gravatai.iOS
{
    public class LocationPermissionVerifier : ILocationPermissionVerifier
    {
        public LocationPermissionVerifier()
        {

        }

        public bool IsPermissionGranted()
        {
            return CLLocationManager.LocationServicesEnabled;
        }
    }
}