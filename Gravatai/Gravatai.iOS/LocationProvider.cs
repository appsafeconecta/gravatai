﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Xamarin.Forms;
using UIKit;
using Gravatai.Model.DeviceLocation;

[assembly: Dependency(typeof(Gravatai.iOS.LocationProvider))]
namespace Gravatai.iOS
{
    public class LocationProvider : ILocationProvider
    {
        public bool IsLocationEnabled()
        {
            return true;
        }

        public void OpenLocationSettings()
        {

        }
    }
}