﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Xamarin.Forms;
using UIKit;
using CoreLocation;
using Gravatai.Model.DeviceLocation;
using Gravatai.Model;

[assembly: Dependency(typeof(Gravatai.iOS.LocationTracker))]
namespace Gravatai.iOS
{
    public class LocationTracker : ILocationTracker
    {
        CLLocationManager locationManager;
        DateTime lastLocationChanged;

        public event EventHandler<GeographicLocation> LocationChanged;

        public LocationTracker()
        {
            locationManager = new CLLocationManager();

            // Request authorization for iOS 8 and above
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                locationManager.RequestWhenInUseAuthorization();
            }

            locationManager.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs args) =>
            {
                CLLocationCoordinate2D coordinate = args.Locations[0].Coordinate;
                EventHandler<GeographicLocation> handler = LocationChanged;

                if (handler != null && DateTime.UtcNow > lastLocationChanged + TimeSpan.FromMilliseconds(1000))
                {
                    lastLocationChanged = DateTime.UtcNow;
                    handler(this, new GeographicLocation(coordinate.Latitude, coordinate.Longitude));
                }
            };
        }

        public void StartTracking()
        {
            if (CLLocationManager.LocationServicesEnabled)
            {
                locationManager.StartUpdatingLocation();

            }

        }

        public void PauseTracking()
        {
            locationManager.StopUpdatingLocation();
        }
    }
}