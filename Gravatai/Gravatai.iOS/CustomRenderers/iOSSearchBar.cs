﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms.Platform.iOS;
using Gravatai.View.CustomRenderers;
using Gravatai.iOS.CustomRenderers;
using Xamarin.Forms;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(CustomSearchBar), typeof(iOSSearchBar))]
namespace Gravatai.iOS.CustomRenderers
{
    public class iOSSearchBar : SearchBarRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<SearchBar> e)
        {
            base.OnElementChanged(e);
            Control.ShowsCancelButton = false;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == SearchBar.TextProperty.PropertyName)
            {
                Control.Text = Element.Text;
            }

            if (e.PropertyName != SearchBar.CancelButtonColorProperty.PropertyName && e.PropertyName != SearchBar.TextProperty.PropertyName)
                base.OnElementPropertyChanged(sender, e);
        }
    }
}