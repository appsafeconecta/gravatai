﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Gravatai.View.CustomRenderers;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms;
using MapKit;
using Xamarin.Forms.Platform.iOS;
using CoreLocation;
using Gravatai.iOS.CustomRenderers;
using System.ComponentModel;
using Gravatai.Model;
using CoreGraphics;

[assembly: ExportRenderer(typeof(CustomMap), typeof(iOSMap))]
namespace Gravatai.iOS.CustomRenderers
{
    public class iOSMap : MapRenderer
    {
        UIView customPinView;
        CustomMap customMap;
        MKMapView nativeMap;
        MKPolylineRenderer polylineRenderer;

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.View> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                var nativeMap = Control as MKMapView;
                nativeMap.GetViewForAnnotation = null;
                nativeMap.OverlayRenderer = null;
            }

            if (e.NewElement != null)
            {
                customMap = (CustomMap)e.NewElement;
                nativeMap = Control as MKMapView;

                nativeMap.GetViewForAnnotation = GetViewForAnnotation;
                nativeMap.OverlayRenderer = GetOverlayRenderer;

                CLLocationCoordinate2D[] coords = new CLLocationCoordinate2D[customMap.RouteCoordinates.Count];
                int index = 0;
                foreach (var position in customMap.RouteCoordinates)
                {
                    coords[index] = new CLLocationCoordinate2D(position.Latitude, position.Longitude);
                    index++;
                }

                var routeOverlay = MKPolyline.FromCoordinates(coords);
                if (nativeMap.Overlays != null)
                    nativeMap.RemoveOverlays(nativeMap.Overlays);
                nativeMap.AddOverlay(routeOverlay);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals(CustomMap.BusPinsProperty.PropertyName))
            {
                UpdateBusAnnotations();
            }
        }

        MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
        {
            MKAnnotationView annotationView = null;

            if (annotation is MKUserLocation)
                return null;

            var anno = annotation as MKPointAnnotation;
            var customPin = GetCustomPinFromTitle(anno);
            if (customPin == null)
            {
                throw new Exception("Custom pin not found");
            }

            annotationView = mapView.DequeueReusableAnnotation(customPin.Id);
            if (annotationView == null)
            {
                annotationView = new CustomMKAnnotationView(annotation, customPin.Id);
                if (customPin.Representation == PinRepresentation.Bus)
                    annotationView.Image = UIImage.FromFile("bus_icon@3x.png");
                else
                    annotationView.Image = UIImage.FromFile("place_red@3x.png");
                annotationView.CalloutOffset = new CGPoint(0, 0);
                ((CustomMKAnnotationView)annotationView).Id = customPin.Id;
                ((CustomMKAnnotationView)annotationView).Representation = customPin.Representation;
            }
            annotationView.CanShowCallout = true;

            return annotationView;
        }

        /*
        CustomPin GetCustomPinFromPosition(MKPointAnnotation annotation)
        {
            var position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);
            foreach (var stopPin in customMap.StopPins)
            {
                if (stopPin.Pin.Position == position)
                {
                    return stopPin;
                }
            }
            foreach (var busPin in customMap.BusPins)
            {
                if (busPin.Pin.Position == position)
                {
                    return busPin;
                }
            }
            return null;
        }
        */

        void UpdateBusAnnotations()
        {
            foreach (var annotation in nativeMap.Annotations)
            {
                if (!(annotation is MKPointAnnotation))
                    continue;

                var anno = annotation as MKPointAnnotation;
                if (anno == null)
                {
                    throw new Exception("annotation could not be cast to MKPointAnnotation");
                }

                var customPin = GetCustomPinFromTitle(anno);
                if (customPin != null)
                {
                    if (customPin.Representation == PinRepresentation.Bus)
                    {
                        annotation.SetCoordinate(new CoreLocation.CLLocationCoordinate2D(customPin.Pin.Position.Latitude, customPin.Pin.Position.Longitude));
                        anno.Subtitle = customPin.Pin.Address;
                    }
                }
            }
        }

        CustomPin GetCustomPinFromTitle(MKPointAnnotation annotation)
        {
            string title = annotation.GetTitle();
            foreach (var stopPin in customMap.StopPins)
            {
                if (stopPin.Pin.Label.Equals(title))
                {
                    return stopPin;
                }
            }
            foreach (var busPin in customMap.BusPins)
            {
                if (busPin.Pin.Label.Equals(title))
                {
                    return busPin;
                }
            }
            return null;
        }

        MKOverlayRenderer GetOverlayRenderer(MKMapView mapView, IMKOverlay overlay)
        {
            if (polylineRenderer == null)
            {
                polylineRenderer = new MKPolylineRenderer(overlay as MKPolyline);
                polylineRenderer.FillColor = UIColor.Blue;
                polylineRenderer.StrokeColor = UIColor.Red;
                polylineRenderer.LineWidth = 3;
                polylineRenderer.Alpha = 0.4f;
            }
            return polylineRenderer;
        }
    }
}