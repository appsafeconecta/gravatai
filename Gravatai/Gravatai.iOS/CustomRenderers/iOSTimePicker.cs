﻿using Gravatai.iOS.CustomRenderers;
using Gravatai.View.CustomRenderers;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(iOSTimePicker))]
namespace Gravatai.iOS.CustomRenderers
{
    public class iOSTimePicker : TimePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);
            var timePicker = (UIDatePicker)Control.InputView;
            timePicker.Locale = new Foundation.NSLocale("no_nb");
        }
    }
}