﻿using Gravatai.Model;
using MapKit;

namespace Gravatai.iOS.CustomRenderers
{
    public class CustomMKAnnotationView : MKAnnotationView
    {
        public string Id { get; set; }
        public PinRepresentation Representation { get; set; }

        public CustomMKAnnotationView(IMKAnnotation annotation, string id)
            : base(annotation, id)
        {

        }
    }
}