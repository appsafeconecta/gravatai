﻿using MailKit.Net.Smtp;
using MimeKit;
using Gravatai.Model.Mail;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(Gravatai.iOS.MailSender))]
namespace Gravatai.iOS
{
    public class MailSender : IMailSender
    {
        public async Task Send(string name, string email, string type, string subject, string message, string bus, string route, string telephone, bool wantsResponse, DateTime date)
        {
            var mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("Aplicativo SafeConecta iOS", "appsafeconecta@gmail.com"));
            mailMessage.To.Add(new MailboxAddress("semurb.cco@gravatai.rs.gov.br"));
            mailMessage.Subject = "Ocorrência relatada pelo Aplicativo SafeConecta iOS";

            string msgBody = "";
            if (string.IsNullOrWhiteSpace(name) && string.IsNullOrWhiteSpace(email) && string.IsNullOrWhiteSpace(telephone))
            {
                msgBody += "Um usuário anônimo";
            }
            else
            {
                msgBody += "O seguinte usuário: (";
                if (!string.IsNullOrWhiteSpace(name))
                {
                    msgBody += "Nome: " + name;
                }
                if (!string.IsNullOrWhiteSpace(email))
                {
                    if (!string.IsNullOrWhiteSpace(name))
                        msgBody += ", ";
                    msgBody += "E-Mail: " + email;
                }
                if (!string.IsNullOrWhiteSpace(telephone))
                {
                    if (!string.IsNullOrWhiteSpace(name) || !string.IsNullOrWhiteSpace(email))
                        msgBody += ", ";
                    msgBody += "Telefone: " + telephone;
                }
                msgBody += ")";
            }
            msgBody += @" relatou dia " +
                    date.ToString("dd/MM/yyyy") + " às " + date.ToString("HH:mm:ss") + " um(a) " + type +
                    " acerca do(a) " + subject + ".";

            if (!string.IsNullOrWhiteSpace(bus))
            {
                msgBody += @"

Número Ônibus:
" + bus;
            }
            if (!string.IsNullOrWhiteSpace(route))
            {
                msgBody += @"

Linha:
" + route;
            }
            if (!string.IsNullOrWhiteSpace(message))
            {
                msgBody += @"

Mensagem:
" + message;
            }
            if (wantsResponse)
            {
                msgBody += @"

O usuário deseja retorno da Prefeitura.";
            }

            mailMessage.Body = new TextPart("plain")
            {
                Text = msgBody
            };

            try
            {
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    await client.ConnectAsync("smtp.gmail.com", 587);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync("appsafeconecta", "S@f3C0n3ct@2018");

                    await client.SendAsync(mailMessage);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}