﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Gravatai.Model.DeviceLocation;

[assembly: Dependency(typeof(Gravatai.iOS.LocationPermissionRequester))]
namespace Gravatai.iOS
{
    public class LocationPermissionRequester : ILocationPermissionRequester
    {
        public event EventHandler<bool> RequestLocationPermissionResult;

        public void RequestLocationPermission()
        {

        }
    }
}